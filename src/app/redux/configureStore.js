import {createStore, applyMiddleware} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from './rootReducer';

//thunk allows to dispatch several actions in the same action creator fct
import thunk from 'redux-thunk';

//firebase and firestore configuration:
import { reactReduxFirebase, getFirebase } from 'react-redux-firebase';
import { reduxFirestore, getFirestore } from 'redux-firestore';

//import the firebase config for proxipiso project
import firebase from '../config/firebase';

//configure react-redux-firebase'
const rrfConfig = {
  userProfile: 'userProfiles', //name of firestore collection that contains user profiles
  attachAuthIsReady: true,
  useFirestoreForProfile: true, // Firestore for Profile instead of Realtime DB
  updateProfileOnLogin: false,  //for social login
}

export const configureStore = initialState => {

  const middlewares = [thunk.withExtraArgument({getFirebase, getFirestore})];

  const middlewareEnhancer = applyMiddleware(...middlewares);

  const storeEnhancers = [middlewareEnhancer];

  const composedEnhancer = composeWithDevTools(
    ...storeEnhancers,
    reactReduxFirebase(firebase, rrfConfig),
    reduxFirestore(firebase)    
    );

  const store = createStore(rootReducer, initialState, composedEnhancer);

  return store;

}//configureStore
const reducerCreator = (initialState, functionLookupTable) => {
  
  return (state = initialState, {type, payload}) => {
    
    const handler = functionLookupTable[type];

    return handler ? handler(state, payload) : state

  }

}//reducerCreator

export default reducerCreator;

/**
 * Later we will define for example
 * const testReducer = reducerCreator(initialState, {
 * [INCREMENT_COUNTER]: incrementCounterFunction,
 * [DECREMENT_COUNTER]: decrementCounterFunction,
 * })
 * 
 * So this is going to be a functionLookupTable with 
 * INCREMENT_COUNTER and DECREMENT_COUNTER being the type
 * and the provided functions being the handlers.
 * 
 * For example, if we take functionLookupTable[DECREMENT_COUNTER],
 * the handler will be the decrementCounterFunction. In that 
 * case we pass the state and payload to the decrementCounterFunction
 * which itself returns the corresponding action object. And if
 * no handler will be passed, then we simply return the entire state.
 * 
 */
import {combineReducers} from 'redux';
import { reducer as formReducer } from 'redux-form';
import {reducer as toastrReducer} from 'react-redux-toastr';
import modalReducer from '../../features/modals/modalReducer';
import authReducer from '../../features/auth/authReducer';
import asyncReducer from '../../features/async/asyncReducer';
import { firebaseReducer } from 'react-redux-firebase';
import { firestoreReducer } from 'redux-firestore';


import flatReducer from '../../features/flat/flatReducer/flatReducer';

const rootReducer = combineReducers({
  firebase: firebaseReducer,
  firestore: firestoreReducer,
  flats: flatReducer,
  form: formReducer,
  toastr: toastrReducer,
  modal: modalReducer,
  auth: authReducer,
  async: asyncReducer
});//combineReducers

export default rootReducer;
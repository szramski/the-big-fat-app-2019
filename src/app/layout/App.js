import React, {useState} from 'react';
import { Switch, Route } from 'react-router-dom';
import Script from 'react-load-script';

//=====================================
import './App.css';

//=====================================
import ModalManager from '../../features/modals/ModalManager';
import NavBar from '../../features/nav/NavBar/NavBar';
import FlatDashboard from '../../features/flat/FlatDashboard/FlatDashboard';
import FlatDetailedPage from '../../features/flat/flatDetailed/FlatDetailedPage/FlatDetailedPage';
import FlatForm from '../../features/flat/FlatForm/FlatForm';
import HomePage from '../../features/home/HomePage/HomePage';
import TestComp from '../../testArea/TestComp';
import MyFlats from '../../features/user/MyFlats/MyFlats';

const MAPS_API_KEY = 'AIzaSyDImUmR1lCEquHDAKssEn_7eEH2L2IRU80';

//=====================================

const App = () => {

  const [isGoogleApiScriptLoaded, setIsGoogleApiScriptLoaded] = useState(false);

  const handleScriptLoaded = () => setIsGoogleApiScriptLoaded(true);

  return (
    <div>
        <Script
        url={`https://maps.googleapis.com/maps/api/js?key=${MAPS_API_KEY}&libraries=places`}
        onLoad={handleScriptLoaded}
      />
      {
        //render this field only if the API script as been loaded
        isGoogleApiScriptLoaded &&         
    <div>
      <ModalManager/>      
        <Switch>
          <Route path='/' component={HomePage} exact />
        </Switch>

      <Route 
        path="/(.+)"
        render={() => (
          <div>
            <NavBar/>
              <Switch>
                <Route path='/feed'                 component={FlatDashboard} />
                <Route path='/flat/:id'             component={FlatDetailedPage} />
                <Route path='/flatform/draft/:id'   component={FlatForm} />
                <Route path='/flatform'             component={FlatForm} />
                <Route path='/myans'                component={MyFlats} />
                <Route path='/test'                 component={TestComp} />
              </Switch>
          </div>          
        )}//render
      />

    </div>
    }
    </div>
        
  )//return
}

export default App;

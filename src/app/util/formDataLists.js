//import moment from 'moment'; //for initialFlat

export const typesOfProperty = [
  { value: 'Flat',                  key: 1, text: 'Flat' },
  { value: 'Room',                  key: 2, text: 'Room' },
  { value: 'Room in a shared flat', key: 3, text: 'Room in a shared flat' },
  { value: 'Attic flat',            key: 4, text: 'Attic flat' },
  { value: 'Studio appartment',     key: 5, text: 'Studio Appartment' },
  { value: 'Ground floor',          key: 6, text: 'Ground Floor' },//planta Baja, Erdgeschoss
  { value: 'House',                 key: 7, text: 'House' },
  { value: 'Maisonette',            key: 8, text: 'Maisonette' }, //Duplex
];    

export const typesOfFurnishing = [
  { value: 'Unfurnished',       key: 1,  text: 'Unfurnished' },
  { value: 'Rooms and kitchen', key: 2,  text: 'Furnished: Rooms & Kitchen' },
  { value: 'Rooms only',        key: 3,  text: 'Furnished: Rooms Only' },
  { value: 'Kitchen only',      key: 4,  text: 'Furnished: Kitchen Only' },
];   

export const typesOfSolarOrientation = [
  { value: 'north', key: 1, text: 'north' },
  { value: 'east',  key: 2, text: 'east' },
  { value: 'west',  key: 3, text: 'west' },
  { value: 'south', key: 4, text: 'south' },
  { value: '-', key: 5, text: '-' },     
];   


export const typesOfEpcRating = [
  { value: 'A', key: 1, text: 'A' },
  { value: 'B', key: 2, text: 'B' },
  { value: 'C', key: 3, text: 'C' },
  { value: 'D', key: 4, text: 'D' },
  { value: 'E', key: 5, text: 'E' },
  { value: 'F', key: 6, text: 'F' },
  { value: 'G', key: 7, text: 'G' },
  { value: 'Currently Being Obtained',  key: 8, text: 'Currently Being Obtained' },
  { value: 'EPC Not Required',          key: 9, text: 'EPC not required' },
];

export const typesOfCurrency = [
  { value: 'EUR', key: 1, text: 'EUR' },
  { value: 'PLN', key: 2, text: 'PLN' },
  { value: 'GBP', key: 3, text: 'GBP' },
];  

export const typesOfPayment = [
  { value: 'month', key: 0, text: 'month' },
  { value: 'week', key: 1, text: 'week' },
  { value: 'year', key: 2, text: 'year' }, 
  { value: '-', key: 3, text: '-' },   
]; 

export const typesOfDeposit = [
  { value: 'No Deposit', key: 0, text: 'No Deposit' }, //Monatsrate, mensualidad
  { value: '1 month rate', key: 1, text: '1 month rate' }, 
  { value: '2 month rates', key: 2, text: '2 month rates' }, 
  { value: '3 month rates', key: 3, text: '3 month rates' }, 
  { value: '4 month rates', key: 4, text: '4 month rates' }, 
  { value: '5 month rates', key: 5, text: '5 month rates' }, 
  { value: '6 month rates', key: 6, text: '6 month rates' }, 
  { value: 'more than 6 monthly rates', key: 7, text: 'more than 6 monthly rates' }, 
];   

export const initialFlat = {
  propertyType: "Flat",
  numberOfBedrooms: 2,
  numberOfBathrooms: 1,        
  floor: 1,
  floorTotal: 4,
  surface: 30,
  surfaceNet: 30,    
  availableFrom: '01-01-2019',
  epcRatingType: "Currently Being Obtained",
  solarOrientation: "-",
  furnishingType: "Unfurnished",
  yearConstruction: "-",
  yearRenovation: "-",
  rent: 400,
  rentCurrency: 'EUR',
  paymentPeriod: 'month',
  deposit: 'No Deposit',
  additionalCosts: 0,
  additionalCostsPaymentPeriod: '-',
  flatImages: [],
  headline: '',
  description: '',
  referenceId: '',
  publishStreetNumber: false,
  prefStudentsAllowed: false, 
  prefPetsAllowed: false,
  prefSmokersAllowed: false,
  ftrBalcony: false,
  ftrAirConditioning: false,
  ftrHeating: false,
  ftrBuiltInClosets: false,
  ftrReinforcedDoor: false,
  ftrParquetFlooring: false,
  ftrFridge: false,
  ftrStove: false,
  ftrMicrowave: false,
  ftrWashingMachine: false,
  ftrDishwasher: false,
  ftrTV: false,
  ftrGarage: false,
  ftrBasement: false,
  ftrElevator: false,
  ftrPool: false,
  ftrGarden: false,
  ftrPlayground: false,
  addressSearchString: '',
  address_details: {
    streetNumber: '',
    streetName: '',
    city: '',
    postalcode: '',
    region: '',
    country: '',
    country_short: '',
    formattedAddress: '',
    latlng: { lat: 0, lng: 0}
  }// address_details
};
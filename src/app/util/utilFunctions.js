/** get array of flats from localStorage */
export const getFlatArrayFromLocalStorage = () => {

  let a = [];

  let storeData = localStorage.getItem('ppFlatDraftArray');

  if (storeData){
    a = JSON.parse(storeData);
  }

  return a;

}

/** set array of flats in localStorage */
export const setFlatArrayInLocalStorage = (a) => 
  localStorage.setItem('ppFlatDraftArray', JSON.stringify(a));
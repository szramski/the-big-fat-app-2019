import React from 'react';
import { Checkbox as SemUICheckbox, Icon } from 'semantic-ui-react';
import { FormGroup, Label } from "reactstrap";
import classnames from 'classnames';

const CheckBox = ({ input, hint, label, showborder = false, checkBoxText, meta: { touched, error } }) => {

  return (
    <FormGroup className={classnames({'p-1 border shadow-sm': showborder})}>
    {label && <Label className="pr-0 py-1 pl-2 m-0">{label}</Label>}
      <SemUICheckbox

      slider
      defaultChecked={!!input.value}
      onChange={(e, data) => input.onChange(data.checked)}
      type="checkbox"
      label={checkBoxText}
      className="p-2"
    />
    {' '}
    {
      input.value ? 
      <span className="text-success px-2">
        <Icon name="check" />
      </span>  
    : 
      <span className="text-danger px-2">
        <Icon name="ban" />
      </span>      
    }
    {hint && <small className="p-0 pl-2 m-0 mt-2 form-text text-muted">{hint}</small>}
    {error && <div className="invalid-feedback p-0 pl-2">{error}</div>}
  </FormGroup>
  )
}

export default CheckBox;

//See: https://stackoverflow.com/questions/44535840/unable-to-make-the-checkbox-work-with-redux-form-and-react-semantic-ui


import React from 'react';
import { FormGroup, Label} from "reactstrap";
import classnames from 'classnames';
import PlacesAutocomplete from 'react-places-autocomplete';

import './PlaceField.scss';

const PlaceField = ({ input, label, hint, onSelect, placeholder, options, maps_api_key, meta:{error} }) => {
return (
    <FormGroup className="p-1 border shadow-sm">
    { label && <Label className="pr-0 py-1 pl-2 m-0">{label}</Label> }
        <PlacesAutocomplete
          inputProps={{ ...input, placeholder }}
          options={options}
          onSelect={onSelect}
          className={classnames('form-control py-0 px-2', { 'is-invalid': !!error })}
          styles={{
            autocompleteContainer: {
              zIndex: 1000
            }
          }}
        />
        { hint && <small className="p-0 pl-2 m-0 mt-2 form-text text-muted">{hint}</small> }
        {
        //There's a problem with the bootstrap invalid-feedback class so that I
        //must use a workaround...
        error && <div
          className="p-0 pl-2"
          style={{
            color: '#dc3545',
            marginTop: '.25rem',
            fontSize: '80%'
          }}
        >{error}</div>
      }
      </FormGroup>
  )//return

};

export default PlaceField;
import React from 'react';
import { FormGroup, Label } from "reactstrap";
import classnames from 'classnames';

const TextArea = ({ input, hint, rows, label, placeholder, meta: { error } }) => (
  <FormGroup className="p-1 border shadow-sm">
    {label && <Label className="pr-0 py-1 pl-2 m-0">{label}</Label>}
    <textarea
      {...input}
      placeholder={placeholder}
      rows={rows}
      className={classnames('form-control py-0 pt-2 px-2', { 'is-invalid': !!error })}
    >
    </textarea>
    {hint && <small className="p-0 pl-2 m-0 mt-2 form-text text-muted">{hint}</small>}
    {error && <div className="invalid-feedback p-0 pl-2">{error}</div>}
  </FormGroup>
)

export default TextArea;
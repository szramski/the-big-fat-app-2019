import React from 'react';
import { FormGroup, Label } from "reactstrap";
import NumericInput from 'react-numeric-input';
import classnames from 'classnames';

const NumInput = ({ input, label, min, max, hint, meta: { error } }) => {

  return (
    <FormGroup className="p-1 border shadow-sm">
      {label && <Label className="p-0 py-1 pl-2 m-0">{label}</Label>}

      <NumericInput
        min={min}
        max={max}
        type="text"
        value={input.value}
        onChange={input.onChange}
        className={classnames('form-control py-0 px-2', { 'is-invalid': !!error })}
        mobile
        style={{
          wrap: {
            width: '100%',
            height: '38px',
            borderRadius: '6px 3px 3px 6px',
            fontSize: '48px !important'
          },
          input: {
            width: '100%',
            height: '100%',
            paddingRight: '30px',
            borderRadius: '4px 2px 2px 4px',
            color: '#000',
            border: '1px solid #ccc',
            display: 'block',
            fontWeight: '100 !important',
          },
          'input:focus': {
            border: '1px inset #B0BEC5',
            cursor: 'none'
          },
          'btnDown.mobile': {
            width: '40px',
            background: '#B0BEC5',
            cursor: 'pointer'
          },
          'btnUp.mobile': {
            width: '40px',
            background: '#B0BEC5',
            cursor: 'pointer'
          }
        }}  
        />
      {hint && <small className="p-0 pl-2 m-0 form-text text-muted">{hint}</small>}
      {
        //There's a problem with the bootstrap invalid-feedback class so that I
        //must use a workaround...
        error && <div
          className="p-0 pl-2"
          style={{
            color: '#dc3545',
            marginTop: '.25rem',
            fontSize: '80%'
          }}
        >{error}</div>
      }
    </FormGroup>
  )
}

export default NumInput;


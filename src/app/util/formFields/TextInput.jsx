import React from 'react';
import { FormGroup, Label } from "reactstrap";
import classnames from 'classnames';

const TextInput = ({ input, hint, type, label, placeholder, meta: { error, touched } }) => (
  <FormGroup className="p-1 border shadow-sm">
    {label && <Label className="pr-0 py-1 pl-2 m-0">{label}</Label>}
    <input
      {...input}
      type={type}
      placeholder={placeholder}
      autoComplete="off"
      className={classnames('form-control py-0 px-2', { 'is-invalid': !!error })}
    />
    {hint && <small className="p-0 pl-2 m-0 mt-2 form-text text-muted">{hint}</small>}
    {touched && error && <div className="invalid-feedback p-0 pl-2">{error}</div>}
  </FormGroup>
)

export default TextInput;
import React, {useState} from 'react';
import { FormGroup, Label } from "reactstrap";
import { SingleDatePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment';


const DateInput = ({input: {value, name, onChange, onBlur, ...restInput}, change, label, hint,
                    width, placeholder, meta: {touched, error}, ...rest}) => {

const [date, setDate] = useState(undefined);
const [focused, setFocused] = useState(false);

const handleDateChange = (date) => {
  setDate(date);
  change(name, date)
}

  return (
    <FormGroup className="p-1 border shadow-sm">
    { label && <Label className="pr-0 py-1 pl-2 m-0">{label}</Label> }
      <SingleDatePicker
          date={value ? moment(value) : date} // momentPropTypes.momentObj or null
          onDateChange={handleDateChange} // PropTypes.func.isRequired
          focused={focused} // PropTypes.bool
          onFocusChange={ ({ focused }) => setFocused(focused)} // PropTypes.func.isRequired
          numberOfMonths={1}
          block={true}
          noBorder={true}
          isOutsideRange={() => false}
          withPortal={true}
          displayFormat={() => moment.localeData().longDateFormat('LL')}
          
        />
      { hint && <small className="p-0 pl-2 m-0 mt-2 form-text text-muted">{hint}</small> }
    </FormGroup>
  )
}

export default DateInput;

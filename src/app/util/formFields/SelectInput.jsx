import React from 'react';
import { FormGroup, Label} from "reactstrap";
import { Select } from 'semantic-ui-react';
//import classnames from 'classnames';

const SelectInput = ({input, placeholder, label, multiple, options, hint, meta: {error}}) => {
  return (
    <FormGroup className="p-1 border shadow-sm">
    { label && <Label className="pr-0 py-1 pl-2 m-0">{label}</Label> }
      <Select
        value={input.value || null}
        onChange={ (event, data) => input.onChange(data.value) }
        placeholder={placeholder}
        options={options}
        multiple={multiple}
        search
        selection
        style={{minWidth: '100%', width: '100%'}}
        //className={ classnames('form-control py-0 px-2', {'is-invalid': !!error })}
      />
    { hint && <small className="p-0 pl-2 m-0 mt-2 form-text text-muted">{hint}</small> }
    { error && <div className="invalid-feedback p-0 pl-2">{error}</div> }
  </FormGroup>
  )
}

export default SelectInput;

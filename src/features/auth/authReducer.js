import * as authConstants from './authConstants';
import reducerCreator from '../../app/redux/reducerCreator';

const initialState = {
  currentUser: {}
}

export const loginUser = (state, payload) => {
  return {
    ...state,
    authenticated: true,
    currentUser: payload.creds.email
  }
}

export const logoutUser = (state, payload) => {
  return {
    ...state,
    authenticated: false,
    currentUser: {}
  }
}

export default reducerCreator(initialState, {
  [authConstants.LOGIN_USER]: loginUser,
  [authConstants.LOGOUT_USER]: logoutUser
})
import React from 'react';
import {Form, Segment, Button, Divider} from 'semantic-ui-react';
import {Field, reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {login, socialLogin} from '../authActions'
import TextInput from '../../../app/util/formFields/TextInput';
import SocialLogin from '../SocialLogin/SocialLogin';

const actions = { login, socialLogin };

const LoginForm = ({login, socialLogin, handleSubmit, error}) => (
  <Form error size='large' onSubmit={handleSubmit(login)} >
    <Segment>
      <Field 
        name='email'
        component={TextInput}
        type='text'
        placeholder='Email Address'
      />
      <Field 
        name='password'
        component={TextInput}
        type='password'
        placeholder='Password'
      />      
      {error && <div className="text-danger text-center my-2 p-0 pl-2">{error}</div>}
      <Button 
        fluid 
        size='large' 
        color='teal' 
        content='Login'
      />
      <Divider horizontal>
        or
      </Divider>
      <SocialLogin socialLogin={socialLogin} />
    </Segment>
  </Form>
)

export default connect(null, actions)(
  reduxForm({
    form: 'loginForm'
  })(LoginForm)
)
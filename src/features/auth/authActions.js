import { closeModal } from '../modals/modalActions';
import { SubmissionError } from 'redux-form';

//=============================================
//=============================================

export const login = ({ email, password }) => {

  const handleFirebaseLogin = async (dispatch, getState, { getFirebase }) => {

    const firebaseAuth = getFirebase().auth();

    try {

      await firebaseAuth.signInWithEmailAndPassword(email, password);

    } catch (loginError) {

      let errorMessage = '';

      switch (loginError.code) {
        case 'auth/user-not-found':
          errorMessage = 'User not found.';
          break;
        case 'auth/invalid-email':
          errorMessage = 'Invalid email.';
          break;
        case 'auth/argument-error':
          errorMessage = 'Both fields are required.';
          break;
        case 'auth/wrong-password':
          errorMessage = 'Invalid password.';
          break;
        case 'auth/network-request-failed':
          errorMessage = "Can't connect to server.";
          break;
        default:
          errorMessage = 'Unexpected error.';
          break;
      }

      throw new SubmissionError({
        _error: errorMessage
      })
    }

    dispatch(closeModal());

  }//handleFirebaseLogin

  return handleFirebaseLogin;
}//login

//=============================================
//=============================================

export const register = ({email, password, displayName}) => {

  const handleFirebaseRegister = async (dispatch, getState, { getFirebase, getFirestore }) => {

    const firebaseAuth = getFirebase().auth();
    const firestore = getFirestore();

    try{

      let createUserResult = await firebaseAuth.createUserWithEmailAndPassword(email, password);

      /*The above command only creates the new user and tracks his email and password.
        It also logs in the new user directly and stores his data in the auth object of 
        the firebase reducer. But we want to store the user's displayName as well. 
        In order to do that, we need to:
        1.) update the displayName in the current auth object (but it will only be reflected
          as soon as the SPA gets a full refresh)
      */
        await createUserResult.user.updateProfile({
          displayName //shorthand for displayName: displayName
        })
  
      /* 2) create a profile for this user in Firestore */
        let newUser = {
          displayName,
          email,
          photoURL: '/assets/user.png',
          createdAt: firestore.FieldValue.serverTimestamp()
        }
  
        await firestore.set(`userProfiles/${createUserResult.user.uid}`, {
          ...newUser
        })

    } catch(registerError){

      console.log("register error", registerError);

      let errorMessage = '';

      switch (registerError.code) {
        case 'auth/user-not-found':
          errorMessage = 'User not found.';
          break;
        case 'auth/invalid-email':
          errorMessage = 'Invalid email.';
          break;
        case 'auth/argument-error':
          errorMessage = 'All fields are required.';
          break;
        case 'auth/weak-password':
          errorMessage = 'The password must have at least 6 characters.';
          break;
        case 'auth/network-request-failed':
          errorMessage = "Can't connect to server.";
          break;
        case 'auth/email-already-in-use':
          errorMessage = "The email address is already in use.";
          break;       
        default:
          errorMessage = 'Unexpected error.';
          break;
      }

      throw new SubmissionError({
        _error: errorMessage
      })
    }

    dispatch(closeModal());
    
  }//handleFirebaseRegister

  return handleFirebaseRegister;
}//register

//=============================================
//=============================================

export const socialLogin = provider => {

  const someFunction = async (dispatch, getState, {getFirebase, getFirestore}) => {

    try{

      //Close our Modal because the modal of the provider will appear
      dispatch(closeModal());

      const loginResult = await getFirebase().login({
        provider,
        type: 'popup'
      })

      /*The above login function logs in the user via the social provider
        and it also creates a profile for the user. But it uses stuff like
        avatarURL instead of photoURL so that we want to write our own profile
        for the user. But registration and login via social providers are the same
        functionality. Only difference is that when the user logs in first, the
        isNewUser property will be set to true, otherwise to false. Only if it is
        true, we will write the user profile. Otherwise, it's going to be a simply
        login via social network.
      */

      if(loginResult.additionalUserInfo.isNewUser){

        let newUser = {
          displayName: loginResult.profile.displayName,
          email: loginResult.profile.email,
          photoURL: loginResult.profile.avatarUrl,
          createdAt: getFirestore().FieldValue.serverTimestamp()
        }
  
        await getFirestore().set(`userProfiles/${loginResult.user.uid}`, {
          ...newUser
        })

      }//if


    } catch(error) {
      console.log(error)
    }

  }//someFunction

  return someFunction

}//socialLogin
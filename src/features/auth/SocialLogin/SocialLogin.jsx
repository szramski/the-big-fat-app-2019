import React from 'react';
import {Button, Icon} from 'semantic-ui-react';

const SocialLogin = ({socialLogin}) => (
  <div>
    <Button 
      type='button' 
      fluid 
      color='facebook'
      className="mb-3"
      onClick={ () => socialLogin('facebook') }
      >
        <Icon name='facebook' />
        Enter with Facebook 
      </Button>
    <Button 
      type='button' 
      fluid 
      color='google plus'
      onClick={ () => socialLogin('google') }      
    >
      <Icon name='google' />
      Enter with Google
    </Button>
  </div>
)

export default SocialLogin;

import React from 'react';
import { Form, Segment, Button, Divider } from 'semantic-ui-react';
import { Field, reduxForm } from 'redux-form';
import {
  createValidator, 
  combineValidators, 
  composeValidators, 
  isRequired, 
  hasLengthGreaterThan
} from 'revalidate';
import TextInput from '../../../app/util/formFields/TextInput';
import {connect} from 'react-redux';
import {register, socialLogin} from '../authActions';
import SocialLogin from '../SocialLogin/SocialLogin';

const actions = { register, socialLogin };

const isValidEmail = createValidator(
  message => value => {
    if (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
      return message
    }
  },
  'Invalid email address'
)

const validate = combineValidators({
  displayName: composeValidators(
    isRequired({message: 'Required'}),
    hasLengthGreaterThan(2)({message: 'At least 3 characters'})
  )(),
  email: composeValidators(
    isRequired({message: 'Required'}), 
    isValidEmail()
  )(),
  password: composeValidators(
    isRequired({message: 'Required'}),
    hasLengthGreaterThan(5)({message: 'At least 6 characters'})
  )()
});

const RegisterForm = ({register, socialLogin, handleSubmit, error, invalid, submitting}) => (
  <Form error size='large' onSubmit={handleSubmit(register)}>
    <Segment>
      <Field
        name='displayName'
        component={TextInput}
        type='text'
        placeholder='Known As'
      />
      <Field
        name='email'
        component={TextInput}
        type='text'
        placeholder='Email Address'
      />
      <Field
        name='password'
        component={TextInput}
        type='password'
        placeholder='Password'
      />
      {error && <div className="text-danger text-center my-2 p-0 pl-2">{error}</div>}
      <Button
        fluid
        disabled={invalid || submitting}
        size='large'
        color='teal'
        content='Register'
      />
      <Divider horizontal>
        or
      </Divider>
      <SocialLogin socialLogin={socialLogin}/>      
    </Segment>
  </Form>
)

export default connect(null, actions)(
  reduxForm({
    form: 'registerForm',
    validate
  })(RegisterForm)
)
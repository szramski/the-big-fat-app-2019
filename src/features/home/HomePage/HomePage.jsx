import React from 'react';
import { Link } from 'react-router-dom';
import { HashLink } from 'react-router-hash-link';

import { Container, Row, Col } from "reactstrap";
import { Image } from 'semantic-ui-react';



import "./HomePage.scss";

const HomePage = () => {

  return (
  <Container fluid className="hero bg-success">
    <Row className="align-items-center text-center h-100">
      <Col md="12" className="">
        <div className="szr-welcome text-white">
          Make yourself at home.
        </div>
        <HashLink smooth to="/#more" className="btn btn-lg btn btn-outline-light mt-5 mr-3">
          More Info
        </HashLink>        
        <Link to="/feed" className="btn btn-lg btn btn-outline-light mt-5">
          Enter
        </Link>
      </Col>
    </Row>
    <Row className="text-center h-100 py-2 bg-white" id="more">
      <Col md="6" className="">
        <Image src="/assets/home/peter_szrama.jpg" fluid/>
      </Col>
      <Col md="6" className="">
      <div>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Est asperiores, error explicabo repellat ducimus doloremque ratione quisquam vero nam molestiae veritatis voluptatibus velit reiciendis obcaecati dolores id perferendis blanditiis repellendus distinctio laborum magnam. Eius, quis ad dolore aliquam dolor beatae tempore iusto commodi laudantium quidem harum repudiandae, quae sapiente, deserunt ipsam porro consequuntur! Doloribus fuga qui commodi. Deserunt repudiandae similique enim iusto natus ex consectetur, fugit aspernatur architecto praesentium? Quod quasi, dolor odio fugiat beatae officia illo veritatis nulla ut placeat minima quam tempore repudiandae maiores similique! Facere neque dolore voluptate tempora fugiat molestiae dicta officia nemo soluta temporibus quaerat nam labore, et at incidunt, eveniet, numquam fuga a!        
      </div>
      </Col>
    </Row>   
  </Container>
  )//return
};

export default HomePage;
import reducerCreator from '../../app/redux/reducerCreator';
import {ASYNC_ACTION_START, ASYNC_ACTION_FINISH, ASYNC_ACTION_ERROR} from './asyncConstants';

const initialState = {
  loading: false
}

const asyncActionStarted = state => ({...state, loading: true});

const asyncActionFinished = state => ({...state, loading: false});

const asyncActionError = state => ({...state, loading: false});

export default reducerCreator(initialState, {
  [ASYNC_ACTION_START]: asyncActionStarted,
  [ASYNC_ACTION_FINISH]: asyncActionFinished,
  [ASYNC_ACTION_ERROR]: asyncActionError
})
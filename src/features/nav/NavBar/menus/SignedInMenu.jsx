import React from "react";
import { Menu, Image, Dropdown } from "semantic-ui-react";
import { Link } from 'react-router-dom';

const SignedInMenu = ({ currentUserProfile: { displayName, photoURL }, handleSignOut}) => {
  return (
    <Menu.Item className="p-0" position="right">
      <Image avatar spaced="right" src={photoURL || '/assets/user.png'} />
      <Dropdown
        pointing="top right" //muss "top right" sein, um nicht aus dem Rahmen zu gehen!!!
        text={displayName} 
        className="text-white mr-2"
      >
        <Dropdown.Menu>
          <Dropdown.Item text="New Announcement" icon="plus" as={Link} to='/flatform' />
          <Dropdown.Item text="My Announcements" icon="announcement" as={Link} to='/myans' />
          <Dropdown.Item text="My Favorites" icon="heart" />
          <Dropdown.Item text="My Profile" icon="user" as={Link} to={`/profile/1223`} />
          <Dropdown.Item text="Settings" icon="settings" as={Link} to="/settings" />
          <Dropdown.Item text="Sign Out" icon="power" onClick={handleSignOut} />
        </Dropdown.Menu>
      </Dropdown>
    </Menu.Item>
  );
};

export default SignedInMenu;
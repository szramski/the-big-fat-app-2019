import React from 'react';
import { Menu } from 'semantic-ui-react';

const SignedOutMenu = ({handleSignIn, handleSignUp}) => {
  return (
    <Menu.Item position='right'>
      <button 
        className="btn btn-outline-light mr-2"
        onClick={handleSignUp}
      >
        Register
      </button>    
      <button 
        className="btn btn-outline-light"
        onClick={handleSignIn}
      >
        Login
      </button>
    </Menu.Item>
)
}

export default SignedOutMenu;
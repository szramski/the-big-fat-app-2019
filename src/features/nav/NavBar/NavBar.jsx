import React from 'react';
import { Container, Menu } from 'semantic-ui-react';

import { Link, withRouter } from 'react-router-dom';

import {withFirebase} from 'react-redux-firebase';

import {connect} from 'react-redux';
import {openModal} from '../../modals/modalActions'; 

import SignedInMenu from './menus/SignedInMenu';
import SignedOutMenu from './menus/SignedOutMenu';

import './NavBar.scss';

const actions = { openModal }

const mapState = state => ({
  //                   Firebase auth works:             user object exists after login:
  isUserAuthenticated: state.firebase.auth.isLoaded && !state.firebase.auth.isEmpty,
  currentUserProfile: state.firebase.profile, //for displayName and photoURL
})


const NavBar = ({openModal, firebase, currentUserProfile, isUserAuthenticated, history}) => {

  const handleSignIn = () => openModal('LoginModal', null);

  const handleSignUp = () => openModal('RegisterModal', null);

  const handleSignOut = () => {
    firebase.logout();
    history.push('/feed');
  }

  return (
    <Menu fixed="top" className="navbar" >
      <Container className="p-0">
        <Menu.Item as={Link} to="/feed" floated="left" className="p-1 text-white h1 szr-proxipiso">
          Proxipiso
        </Menu.Item>
        {
          isUserAuthenticated ? 
            <SignedInMenu 
              currentUserProfile={currentUserProfile}
              handleSignOut={handleSignOut}
            /> 
            : 
            <SignedOutMenu
            handleSignIn={handleSignIn} 
            handleSignUp={handleSignUp}
            />
        }
      </Container>
    </Menu>
  )//return
}

export default withFirebase(
  connect(mapState, actions)(
    withRouter(NavBar)
  )
);
import React from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { connect } from 'react-redux';

import RegisterForm from '../auth/Register/RegisterForm';
import { closeModal } from "./modalActions";

const actions = { closeModal };

const RegisterModal = ({closeModal}) => (
    <Modal isOpen={true} toggle={closeModal} size="sm">
        <ModalHeader toggle={closeModal}>
            Sign Up
        </ModalHeader>
        <ModalBody>
            <RegisterForm />
        </ModalBody>
    </Modal>
)
export default connect(null, actions)(RegisterModal);
import React from 'react';
import { connect } from 'react-redux';
import LoginModal from './LoginModal';
import RegisterModal from './RegisterModal';

const modalLookup = {
  LoginModal,
  RegisterModal,
}

const mapStateToProps = state => ({
  currentModal: state.modal 
})

const ModalManager = ({currentModal}) => {

  let renderedModal;

  //console.log("ModalMagaer running", currentModal)

  if(currentModal) {
    const { modalType, modalProps } = currentModal;
    const ModalComponent = modalLookup[modalType];

    renderedModal = <ModalComponent {...modalProps}  />;

  }

  //console.log("renderedModal im Manager, ", renderedModal)

  return <span>{renderedModal}</span>
  
}

export default connect(mapStateToProps)(ModalManager);

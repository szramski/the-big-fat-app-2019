import { MODAL_OPEN, MODAL_CLOSE } from './modalConstants';
import reducerCreator from '../../app/redux/reducerCreator';

const initialState = null;

export const openModal = (state, payload) => {

  const { modalType, modalProps } = payload;

  return {modalType, modalProps}
}

export const closeModal = (state, payload) => {

  return null;
}

export default reducerCreator(initialState, {
  [MODAL_OPEN]: openModal,
  [MODAL_CLOSE]: closeModal
})
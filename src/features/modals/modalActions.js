import { MODAL_OPEN, MODAL_CLOSE } from './modalConstants.js';

export const openModal = (modalType, modalProps) => {
  return {
    type: MODAL_OPEN,
    payload: {
      modalType,
      modalProps
    }
  }//return
}//openModal

export const closeModal = () => {

  return {
    type: MODAL_CLOSE
  }//return
}//closeModal
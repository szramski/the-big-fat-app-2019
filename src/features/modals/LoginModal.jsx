import React from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { connect } from 'react-redux';

import LoginForm from '../auth/Login/LoginForm';
import { closeModal } from "./modalActions";

const actions = { closeModal };

const RegisterModal = ({closeModal}) => (
    <Modal isOpen={true} toggle={closeModal} size="sm">
        <ModalHeader toggle={closeModal}>
            Sign In
        </ModalHeader>
        <ModalBody>
            <LoginForm />
        </ModalBody>
    </Modal>
)
export default connect(null, actions)(RegisterModal);
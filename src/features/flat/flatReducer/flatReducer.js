import reducerCreator from '../../../app/redux/reducerCreator';
import {CREATE_FLAT, UPDATE_FLAT, DELETE_FLAT} from './flatConstants';

const initialFlats = [
  {
    "id": "5b980ff610e3dd5b2e8e906f",    
    "address_details": {
      streetNumber: '2',
      streetName: 'St.Annenstraße',
      city: 'Sankt Wendel',
      postalcode: '66606',
      region: 'Saarland',
      country: 'Deutschland',
      country_short: 'DE',
      formattedAddress: 'St.Annenstraße 2, 66606 St. Wendel, Germany',
      latlng: {
        lat: 49.4686747,
        lng: 7.164729800000032
      },
    },
    "constructionYearKnown": true,
    "hasAdditionalCosts": true,
    "hasAirCon": false,
    "hasBalcony": true,
    "hasBasement": false,
    "hasBuiltInClosets": false,
    "hasDishwasher": false,
    "hasElevator": false,
    "hasFridge": true,
    "hasGarage": true,
    "hasGarden": false,
    "hasHeating": true,
    "hasMicrowave": true,
    "hasParquetFlooring": true,
    "hasPlayground": false,
    "hasPool": false,
    "hasReinforcedDoor": false,
    "hasStove": true,
    "hasTV": false,
    "hasWashingMachine": false,
    "isForPets": false,
    "isForSmokers": false,
    "isForStudents": false,
    "publishStreetNumber": true,
    "renovationYearKnown": false,
    "typeOfProperty": "Studio appartment",
    "furnishingType": "Rooms and kitchen",
    "epcRatingType": "Currently Being Obtained",
    "solarOrientation": "n/a",
    "availableFromDate": {
      "$date": "2018-12-01T00:00:00.000Z"
    },
    "createdOnDate": {
      "$date": "2018-09-11T00:00:00.000Z"
    },
    "publishedOnDate": {
      "$date": "2018-09-11T00:00:00.000Z"
    },
    "country": "de",
    "numberOfRooms": 1,
    "numberOfBathRooms": 1,
    "surfaceTotal": 24,
    "surfaceNet": 24,
    "floor": 1,
    "floorTotal": 6,
    "rent": 490,
    "currency": "EUR",
    "paymentPeriod": "month",
    "deposit": 3,
    "additionalCostsAmount": 120,
    "additionalCostsPaymentMethod": "month",
    "constructionYear": 2018,
    "renovationYear": 2000,
    "flatImages": [
      {
        "url": "/assets/tmp/flatimages/7.jpeg",
        "mainImage": true,
      },
      {
        "url": "/assets/tmp/flatimages/8.jpeg",
        "mainImage": false,
      },   
    ],
    "flatLikes": [],
    "flatViewed": [],
    "headline": "Mercurius Real Estate AG",
    "descr_short": "Ausstattung\n- Pantry-Küche\n- Teilmöbliert\n- Einzelbett inklusive Matratze\n- Kleiderschrank \n- Tisch mit zwei Stühlen\n- Fußbodenheizung \n- Hochwertiger Parkettboden\n- Nahezu jede Wohnung verfügt über einen Balkon oder eine Loggia\n- Frankfurts größter Dachgarten (barrierefrei zugänglich)\n- Tiefgaragenstellplätze zur Anmietung ab 90 EUR (monatlich)\n\nMehr Informationen finden Sie auf www.brightside-ffm.de",
    "descr_long": "Lage\nDas Gallusviertel besticht, neben seinen vielfältigen Ausgehmöglichkeiten, durch Kulturstätten wie das \u201eGallustheater\u201c. Auch die Angebote in der Frankfurter Innenstadt sind von der Weilburger Straße aus bestens erreichbar. Bright Side profitiert von einer zentralen Lage mit guter Verkehrsanbindung:\n\n\u2022 nur fünf Minuten zum Main und in zehn Minuten in die City mit ÖPNV oder Fahrrad\n\u2022 zwei Minuten Fahrtzeit bis zum Hauptbahnhof mit der nahe gelegenen S-Bahn oder Straßenbahn\n\u2022 nur zehn Minuten mit der S-Bahn nach Eschborn",
    "creator": {
      "displayName": "Hans Obergruber"
    },
    "publishAnnouncement": true
  },
]

// =================================================
// =================================================

export const createFlat = (state, payload) => {
  //Return the previously existing flat array and
  //add another object out of the new flat data.
  return [...state, Object.assign({}, payload.flat)]
}; // createFlat

// =================================================
// =================================================

export const updateFlat = (state, payload) => {
  //Return the previously existing flat array but 
  //remove the one that we want to update and
  //add another object out of the updated flat data.  
  return [
    ...state.filter( flat => flat.id !== payload.flat.id),
    Object.assign({}, payload.flat)
  ]
}; // createFlat

// =================================================
// =================================================

export const deleteFlat = (state, payload) => {
  //Return the previously existing flat array but 
  //remove the one that we want to delete 
  return [
    ...state.filter( flat => flat.id !== payload.flatId)
  ]
}; // createFlat

export default reducerCreator(initialFlats, {
  [CREATE_FLAT]: createFlat,
  [UPDATE_FLAT]: updateFlat,
  [DELETE_FLAT]: deleteFlat
})
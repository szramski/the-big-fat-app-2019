import {toastr} from 'react-redux-toastr';
import moment from 'moment';

import { 
  asyncActionStart, 
  asyncActionFinish, 
  asyncActionError 
} from '../../async/asyncActions';

// ==========================================
export const createFlat = flatObj => 
//we need to return an async function because we are not allowed
//to make the createFlat action creator function an async one.
async (dispatch, getState, {getFirebase, getFirestore}) => {

    const firebase = getFirebase();
    const firestore = getFirestore(); 
    const creatorUid = firestore.auth().currentUser.uid;
    const creatorDisplayName = firestore.auth().currentUser.displayName;
    const creatorPhotoURL = getState().firebase.profile.photoURL;

    //take the flatImages array out of the flatObj:
    let {flatImages, ...flat} = flatObj;

    //Change the availableFrom date from a moment object to an ISO date string:
    //for example to: "2019-01-05T17:36:10+01:00"
    flat.availableFrom = moment(flat.availableFrom).format();

    //adding the creatorInfo and a created Timestamp:
    const flatWithCreatorInfo = {
      ...flat,
      creatorUid,
      creatorDisplayName,
      creatorPhotoURL,
      createdAt: Date.now()
    } 

    try{

      dispatch(asyncActionStart());

      //create the new flat on firestore and save it in the createdFlat variable
      let createdFlat = await firestore.add('flats', flatWithCreatorInfo);

      //define the path under which we're going to store the flat images
      const path = `flat_images/${createdFlat.id}`;
      
      //define an array that will hold the URLs of the uploaded flatImages.
      let flatImageDownloadURLs = [];
      
      //run through each flat image
      for (let image of flatImages){

        //define the file name of the image that is going to be stored
        const options = {
          name: image.tmpId,
        }

        //upload the image and return a snapshot of the uploaded file
        let uploadedFile = await firebase.uploadFile(path, image.blob, null, options);

        //get the downloadURL of the uploaded file.
        let downloadURL = await uploadedFile.uploadTaskSnapshot.ref.getDownloadURL();
  
        //push a new object into the array. The new object will hold the downloadURL
        //within the src property and it will also hold the mainImage boolean:
        flatImageDownloadURLs.push({src: downloadURL, mainImage: image.mainImage});
      }//for
        
      //now add the array flatImageDownloadURLs to the already existing flat object:
      await firestore.update(`flats/${createdFlat.id}`, {
        flatImages: flatImageDownloadURLs
      });

      //next, we add the flatId to a collection that holds all flats of the specific user
      //and we allow for some basic filtering by tracking the country and the city and the
      //referenceId. But first of all, we request the corresponding collection:

      await firestore.get(`flats_per_user/${creatorUid}`);

      //the above command allows us to access the collection via the getState function:
      const userCreatedFlats = getState().firestore.ordered.flats_per_user || [];

      //Now we shape the current flatInfo object
      let flatInfo = {
        flatId: createdFlat.id,
        createdAt: flatWithCreatorInfo.createdAt,
        country_short: flatWithCreatorInfo.address_details.country_short.toLowerCase(),
        city: flatWithCreatorInfo.address_details.city,
        referenceId: flatWithCreatorInfo.referenceId
      }

      //Next, we add that object to the array:
      userCreatedFlats.push(flatInfo);

      //finally, we write the entire collection:



      //TZODODODODODODOD: convert the array to an object because only this way we can store it:



        //belwo fails because userCreatedFlats must be an object and not an array
      await firestore.set(`flats_per_user/${creatorUid}`, userCreatedFlats);

      //and now we create lookuptables for general searches:

      // await firestore.add(`flats_per_country/${flatWithCreatorInfo.address_details.country_short.toLowerCase()}`, {
      //   flatId: createdFlat.id,
      //   creatorUid: creatorUid
      // });
      // await firestore.add(`flats_per_region/${flatWithCreatorInfo.address_details.region}`, {
      //   flatId: createdFlat.id,
      //   creatorUid: creatorUid
      // });
      // await firestore.add(`flats_per_city/${flatWithCreatorInfo.address_details.city}`, {
      //   flatId: createdFlat.id,
      //   creatorUid: creatorUid
      // });     
      
      // await firestore.add(`countries`, { 
      //   country_short: flatWithCreatorInfo.address_details.country_short.toLowerCase() 
      // });

      // await firestore.add(`regions`, { 
      //   region: flatWithCreatorInfo.address_details.region
      // });

      // await firestore.add(`city`, { 
      //   city: flatWithCreatorInfo.address_details.city
      // });      

      toastr.success('Success', 'The announcement has been published');

      dispatch(asyncActionFinish());

    } catch(error) {
      console.log("Error while adding flat", error);
      dispatch(asyncActionError());
      toastr.error('Error', 'The announcement has not been published');
    }
} //createFlat
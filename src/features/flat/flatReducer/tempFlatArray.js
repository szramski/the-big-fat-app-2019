const initialFlats = [
  {
    "id": "5ba272e0aa7b25001338f14f",
    "address_details": {
      streetNumber: '2',
      streetName: 'St.Annenstraße',
      city: 'Sankt Wendel',
      postalcode: '66606',
      region: 'Saarland',
      country: 'Deutschland',
      country_short: 'DE',
      formattedAddress: 'St.Annenstraße 2, 66606 St. Wendel, Germany',
      latlng: {
        lat: 49.4686747,
        lng: 7.164729800000032
      }
    },
    "creator": {
      "displayName": "Hans Obergruber"
    },
    "publishAnnouncement": false,
    "constructionYearKnown": false,
    "hasAdditionalCosts": false,
    "hasAirCon": false,
    "hasBalcony": false,
    "hasBasement": true,
    "hasBuiltInClosets": false,
    "hasDishwasher": false,
    "hasElevator": false,
    "hasFridge": false,
    "hasGarage": true,
    "hasGarden": false,
    "hasHeating": false,
    "hasMicrowave": false,
    "hasParquetFlooring": false,
    "hasPlayground": false,
    "hasPool": true,
    "hasReinforcedDoor": false,
    "hasStove": false,
    "hasTV": false,
    "hasWashingMachine": false,
    "isForPets": false,
    "isForSmokers": false,
    "isForStudents": false,
    "publishStreetNumber": false,
    "renovationYearKnown": false,
    "typeOfProperty": "Room",
    "furnishingType": "Rooms and kitchen",
    "epcRatingType": "B",
    "solarOrientation": "n/a",
    "availableFromDate": {
      "$date": "2018-09-22T00:00:00.000Z"
    },
    "createdOnDate": {
      "$date": "2018-09-19T00:00:00.000Z"
    },
    "publishedOnDate": {
      "$date": "2018-09-19T00:00:00.000Z"
    },
    "country": "es",
    "numberOfRooms": 1,
    "numberOfBathRooms": 1,
    "surfaceTotal": 40,
    "surfaceNet": 35,
    "floor": 0,
    "floorTotal": 4,
    "rent": 400,
    "currency": "EUR",
    "paymentPeriod": "month",
    "deposit": 0,
    "additionalCostsAmount": 100,
    "additionalCostsPaymentMethod": "n/a",
    "constructionYear": 2000,
    "renovationYear": 2000,
    "flatImages": [
      {
        "url": "/assets/tmp/flatimages/2.jpeg",
        "mainImage": false,
      },
      {
        "url": "/assets/tmp/flatimages/9.jpeg",
        "mainImage": true
      },   
    ],
    "flatLikes": [],
    "flatBookmarks": [],
    "flatTrashed": [],
    "flatComments": [],
    "flatViewed": [],
    "headline": "Beautiful appartment for students",
    "descr_short": "ihbhuh",
    "descr_long": "",
    "__v": 0
  },
  {
    "id": "5b98091910e3dd5b2e8e9060",    
    "address_details": {
      "streetNumber": "n/a",
      "streetName": "Paseo de los Tilos",
      "city": "Málaga",
      "postalcode": "29006",
      "region": "Andalucía",
      "country": "España",
      "country_short": "ES",
      "formattedAddress": "Paseo de los Tilos, 29006 Málaga, Spanien"
    },
    "latlng": {
      "lat": 36.71324250000001,
      "lng": -4.4364923000000545
    },
    "constructionYearKnown": true,
    "hasAdditionalCosts": false,
    "hasAirCon": true,
    "hasBalcony": true,
    "hasBasement": false,
    "hasBuiltInClosets": true,
    "hasDishwasher": false,
    "hasElevator": true,
    "hasFridge": true,
    "hasGarage": true,
    "hasGarden": false,
    "hasHeating": false,
    "hasMicrowave": true,
    "hasParquetFlooring": true,
    "hasPlayground": false,
    "hasPool": false,
    "hasReinforcedDoor": true,
    "hasStove": true,
    "hasTV": false,
    "hasWashingMachine": true,
    "isForPets": false,
    "isForSmokers": false,
    "isForStudents": false,
    "publishStreetNumber": false,
    "renovationYearKnown": false,
    "typeOfProperty": "Flat",
    "furnishingType": "Kitchen only",
    "epcRatingType": "Currently Being Obtained",
    "solarOrientation": "south",
    "availableFromDate": {
      "$date": "2018-09-11T00:00:00.000Z"
    },
    "createdOnDate": {
      "$date": "2018-09-11T00:00:00.000Z"
    },
    "publishedOnDate": {
      "$date": "2018-09-11T00:00:00.000Z"
    },
    "country": "es",
    "numberOfRooms": 3,
    "numberOfBathRooms": 1,
    "surfaceTotal": 116,
    "surfaceNet": 108,
    "floor": 11,
    "floorTotal": 11,
    "rent": 1100,
    "currency": "EUR",
    "paymentPeriod": "month",
    "deposit": 0,
    "additionalCostsAmount": 100,
    "additionalCostsPaymentMethod": "n/a",
    "constructionYear": 1960,
    "renovationYear": 2000,
    "flatImages": [
      {
        "url": "/assets/tmp/flatimages/3.jpeg",
        "mainImage": true,
      },
      {
        "url": "/assets/tmp/flatimages/4.jpeg",
        "mainImage": false,
      },   
    ],
    "flatLikes": [
      {
        "_id": {
          "$oid": "5b99392be7b4b50014b44b2b"
        }
      },
      {
        "_id": {
          "$oid": "5b992f6ee7b4b50014b44b0d"
        }
      }
    ],
    "flatBookmarks": [
      {
        "_id": {
          "$oid": "5b93b02aac6b6e25c8bbdafe"
        }
      },
      {
        "_id": {
          "$oid": "5b99392be7b4b50014b44b2b"
        }
      }
    ],
    "flatTrashed": [
      {
        "_id": {
          "$oid": "5ba36cf75c64c30013a88e3b"
        }
      }
    ],
    "flatComments": [
      {
        "_id": {
          "$oid": "5c213105f315e80013b42d39"
        },
        "user": {
          "$oid": "5b93b02aac6b6e25c8bbdafe"
        },
        "createdAt": 1545679108,
        "comment": "Hola"
      }
    ],
    "flatViewed": [
      {
        "_id": {
          "$oid": "5b9f7a06e9b17a1f18de7efe"
        }
      },
      {
        "_id": {
          "$oid": "5b9a259c5288393c288fc348"
        }
      },
      {
        "_id": {
          "$oid": "5b99392be7b4b50014b44b2b"
        }
      },
      {
        "_id": {
          "$oid": "5b992f6ee7b4b50014b44b0d"
        }
      },
      {
        "_id": {
          "$oid": "5b9810d610e3dd5b2e8e9075"
        }
      },
      {
        "_id": {
          "$oid": "5b93b02aac6b6e25c8bbdafe"
        }
      },
      {
        "_id": {
          "$oid": "5b963eefef01cb0014fa10d2"
        }
      }
    ],
    "headline": "Piso en Malaga ,Vialia / La Unión",
    "descr_short": "Piso muy luminoso totalmente amueblado y con cocina equipada en Málaga zona Vialia con 116 m. de superficie y 8 m2 de terraza, salón con aire acondicionado, tres habitaciones, un baño, un aseo, reformado y totalmente listo para entrar a vivir con cocina equipada, suelo de parquet, carpintería exterior de aluminio lacado y orientación sureste y con aparcamiento.",
    "descr_long": "Una undécima planta con vistas totalmente despejadas. El edificio se encuentra adaptado a minusválidos con dos ascensores. En una zona a un paso del centro con muy buenas comunicaciones, cerca de la estación de tren, de la estación de autobuses, con centros comerciales, zonas ajardinadas, centros médicos, colegios, zonas infantiles. Gastos de comunidad: 56 Eur.",
    "creator": {
      "displayName": "Hans Obergruber"
    },
    "__v": 48,
    "publishAnnouncement": true
  },
  {
    "id": "5b98152810e3dd5b2e8e9076",
    "address_details": {
      "streetNumber": "17",
      "streetName": "Charing Cross Road",
      "city": "n/a",
      "postalcode": "WC2H 0EP",
      "region": "England",
      "country": "United Kingdom",
      "country_short": "GB",
      "formattedAddress": "17 Charing Cross Rd, London WC2H 0EP, Vereinigtes Königreich"
    },
    "latlng": {
      "lat": 51.5102742,
      "lng": -0.12860610000007
    },
    "constructionYearKnown": false,
    "hasAdditionalCosts": false,
    "hasAirCon": false,
    "hasBalcony": false,
    "hasBasement": false,
    "hasBuiltInClosets": false,
    "hasDishwasher": false,
    "hasElevator": true,
    "hasFridge": false,
    "hasGarage": false,
    "hasGarden": false,
    "hasHeating": true,
    "hasMicrowave": false,
    "hasParquetFlooring": true,
    "hasPlayground": false,
    "hasPool": false,
    "hasReinforcedDoor": false,
    "hasStove": false,
    "hasTV": false,
    "hasWashingMachine": false,
    "isForPets": false,
    "isForSmokers": false,
    "isForStudents": true,
    "publishStreetNumber": true,
    "renovationYearKnown": false,
    "typeOfProperty": "Flat",
    "furnishingType": "Rooms and kitchen",
    "epcRatingType": "D",
    "solarOrientation": "n/a",
    "availableFromDate": {
      "$date": "2018-10-01T00:00:00.000Z"
    },
    "createdOnDate": {
      "$date": "2018-09-11T00:00:00.000Z"
    },
    "publishedOnDate": {
      "$date": "2018-09-11T00:00:00.000Z"
    },
    "country": "uk",
    "numberOfRooms": 2,
    "numberOfBathRooms": 2,
    "surfaceTotal": 76,
    "surfaceNet": 74,
    "floor": 3,
    "floorTotal": 6,
    "rent": 2816,
    "currency": "GBP",
    "paymentPeriod": "month",
    "deposit": 2,
    "additionalCostsAmount": 100,
    "additionalCostsPaymentMethod": "n/a",
    "constructionYear": 2000,
    "renovationYear": 2000,
    "flatImages": [
      {
        "url": "/assets/tmp/flatimages/5.jpeg",
        "mainImage": true,
      },
      {
        "url": "/assets/tmp/flatimages/6.jpeg",
        "mainImage": false,
      },   
    ],   
    "flatLikes": [
      {
        "_id": {
          "$oid": "5b9a259c5288393c288fc348"
        }
      },
      {
        "_id": {
          "$oid": "5b9810d610e3dd5b2e8e9075"
        }
      }
    ],
    "flatBookmarks": [
      {
        "_id": {
          "$oid": "5b93b02aac6b6e25c8bbdafe"
        }
      },
      {
        "_id": {
          "$oid": "5b9810d610e3dd5b2e8e9075"
        }
      }
    ],
    "flatTrashed": [],
    "flatComments": [
      {
        "_id": {
          "$oid": "5ba26855bf9b420013f4c7df"
        },
        "user": {
          "$oid": "5b963eefef01cb0014fa10d2"
        },
        "createdAt": 1537370209,
        "comment": "Hola, cuando puedo entrar en este piso?"
      }
    ],
    "flatViewed": [
      {
        "_id": {
          "$oid": "5b9f7a06e9b17a1f18de7efe"
        }
      },
      {
        "_id": {
          "$oid": "5b963eefef01cb0014fa10d2"
        }
      },
      {
        "_id": {
          "$oid": "5b9a259c5288393c288fc348"
        }
      },
      {
        "_id": {
          "$oid": "5b9810d610e3dd5b2e8e9075"
        }
      },
      {
        "_id": {
          "$oid": "5b99392be7b4b50014b44b2b"
        }
      },
      {
        "_id": {
          "$oid": "5b93baf71e98e52652aea29c"
        }
      },
      {
        "_id": {
          "$oid": "5b93b02aac6b6e25c8bbdafe"
        }
      }
    ],
    "headline": "2 Bed Flat, London, WC2H, Ref 402037",
    "descr_short": "This apartment is located in the heart of Central London and is located within walking distance to Leicester Square and Piccadilly Stations. \n\nThis 2 double bedroom apartment comprises an open plan kitchen, dining area and lounge.",
    "descr_long": "There are 2 bathrooms, one containing a shower and the other containing a bath tub.\n\nWith porcelain tiles throughout, the apartment offers modern and contemporary accommodation in the perfect location.",
    "creator": {
      "displayName": "Hans Obergruber"
    },
    "__v": 28,
    "publishAnnouncement": true
  },
  {
    "id": "5b980ff610e3dd5b2e8e906f",    
    "address_details": {
      "streetNumber": "29",
      "streetName": "Weilburger Straße",
      "city": "Frankfurt am Main",
      "postalcode": "60326",
      "region": "Hessen",
      "country": "Deutschland",
      "country_short": "DE",
      "formattedAddress": "Weilburger Str. 29, 60326 Frankfurt am Main, Deutschland"
    },
    "latlng": {
      "lat": 50.1003841,
      "lng": 8.641341900000043
    },
    "constructionYearKnown": true,
    "hasAdditionalCosts": true,
    "hasAirCon": false,
    "hasBalcony": true,
    "hasBasement": false,
    "hasBuiltInClosets": false,
    "hasDishwasher": false,
    "hasElevator": false,
    "hasFridge": true,
    "hasGarage": true,
    "hasGarden": false,
    "hasHeating": true,
    "hasMicrowave": true,
    "hasParquetFlooring": true,
    "hasPlayground": false,
    "hasPool": false,
    "hasReinforcedDoor": false,
    "hasStove": true,
    "hasTV": false,
    "hasWashingMachine": false,
    "isForPets": false,
    "isForSmokers": false,
    "isForStudents": false,
    "publishStreetNumber": true,
    "renovationYearKnown": false,
    "typeOfProperty": "Studio appartment",
    "furnishingType": "Rooms and kitchen",
    "epcRatingType": "Currently Being Obtained",
    "solarOrientation": "n/a",
    "availableFromDate": {
      "$date": "2018-12-01T00:00:00.000Z"
    },
    "createdOnDate": {
      "$date": "2018-09-11T00:00:00.000Z"
    },
    "publishedOnDate": {
      "$date": "2018-09-11T00:00:00.000Z"
    },
    "country": "de",
    "numberOfRooms": 1,
    "numberOfBathRooms": 1,
    "surfaceTotal": 24,
    "surfaceNet": 24,
    "floor": 1,
    "floorTotal": 6,
    "rent": 490,
    "currency": "EUR",
    "paymentPeriod": "month",
    "deposit": 3,
    "additionalCostsAmount": 120,
    "additionalCostsPaymentMethod": "month",
    "constructionYear": 2018,
    "renovationYear": 2000,
    "flatImages": [
      {
        "url": "/assets/tmp/flatimages/7.jpeg",
        "mainImage": true,
      },
      {
        "url": "/assets/tmp/flatimages/8.jpeg",
        "mainImage": false,
      },   
    ],
    "flatLikes": [
      {
        "_id": {
          "$oid": "5b99392be7b4b50014b44b2b"
        }
      }
    ],
    "flatBookmarks": [],
    "flatTrashed": [],
    "flatComments": [
      {
        "_id": {
          "$oid": "5b98109210e3dd5b2e8e9074"
        },
        "user": {
          "$oid": "5b93b02aac6b6e25c8bbdafe"
        },
        "createdAt": 1536692370,
        "comment": "Hi, is it possible to rent this flat a little bit later, from January 1st of 2019 ?"
      }
    ],
    "flatViewed": [
      {
        "_id": {
          "$oid": "5ba36cf75c64c30013a88e3b"
        }
      },
      {
        "_id": {
          "$oid": "5b9a259c5288393c288fc348"
        }
      },
      {
        "_id": {
          "$oid": "5b99392be7b4b50014b44b2b"
        }
      },
      {
        "_id": {
          "$oid": "5b992f6ee7b4b50014b44b0d"
        }
      },
      {
        "_id": {
          "$oid": "5b93baf71e98e52652aea29c"
        }
      },
      {
        "_id": {
          "$oid": "5b9810d610e3dd5b2e8e9075"
        }
      },
      {
        "_id": {
          "$oid": "5b93b02aac6b6e25c8bbdafe"
        }
      }
    ],
    "headline": "Mercurius Real Estate AG, MICRO-APARTMENTS / NEUBAU",
    "descr_short": "Ausstattung\n- Pantry-Küche\n- Teilmöbliert\n- Einzelbett inklusive Matratze\n- Kleiderschrank \n- Tisch mit zwei Stühlen\n- Fußbodenheizung \n- Hochwertiger Parkettboden\n- Nahezu jede Wohnung verfügt über einen Balkon oder eine Loggia\n- Frankfurts größter Dachgarten (barrierefrei zugänglich)\n- Tiefgaragenstellplätze zur Anmietung ab 90 EUR (monatlich)\n\nMehr Informationen finden Sie auf www.brightside-ffm.de",
    "descr_long": "Lage\nDas Gallusviertel besticht, neben seinen vielfältigen Ausgehmöglichkeiten, durch Kulturstätten wie das \u201eGallustheater\u201c. Auch die Angebote in der Frankfurter Innenstadt sind von der Weilburger Straße aus bestens erreichbar. Bright Side profitiert von einer zentralen Lage mit guter Verkehrsanbindung:\n\n\u2022 nur fünf Minuten zum Main und in zehn Minuten in die City mit ÖPNV oder Fahrrad\n\u2022 zwei Minuten Fahrtzeit bis zum Hauptbahnhof mit der nahe gelegenen S-Bahn oder Straßenbahn\n\u2022 nur zehn Minuten mit der S-Bahn nach Eschborn",
    "creator": {
      "displayName": "Hans Obergruber"
    },
    "__v": 18,
    "publishAnnouncement": true
  },
]; //initialFlats
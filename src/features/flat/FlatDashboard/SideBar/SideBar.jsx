import React from 'react';
import {
  Card,
  CardBody,
  CardHeader,
} from 'reactstrap';
import {Sticky} from 'semantic-ui-react';

const SideBar = ({contextRef}) => {
  return (
    <Sticky context={contextRef} offset={60}>
      <Card>
        <CardHeader>
          People I'm following
        </CardHeader>
        <CardBody>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem quibusdam atque consectetur suscipit, nemo rem fugiat aperiam officiis optio nisi ipsa asperiores autem porro quia aut placeat dolores quod similique ex quasi pariatur accusamus ut numquam! Animi explicabo deleniti suscipit ipsam obcaecati omnis debitis amet nostrum est eos id, at quis temporibus odit nam molestias ex. Assumenda quod, veniam architecto, ad exercitationem ab ea nesciunt delectus placeat, earum laborum laudantium unde modi! Porro provident aspernatur eligendi, facere at ipsum in voluptas, reprehenderit numquam fuga laborum nobis voluptatem commodi nihil error ratione sed dignissimos eius qui ducimus incidunt perferendis vel? Nam.
        </CardBody>
      </Card>
    </Sticky>
  )
}

export default SideBar;

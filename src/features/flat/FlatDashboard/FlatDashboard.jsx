import React, {useState} from 'react';
import {Container, Row, Col} from 'reactstrap';

import FlatList from './FlatList/FlatList';
import SideBar from './SideBar/SideBar';
import LoadingComponent from '../../../app/layout/LoadingComponent';

//==============================
import {connect} from 'react-redux';
import {firestoreConnect} from 'react-redux-firebase';

import './FlatDashboard.scss';

const mapStateToProps = state => ({
  flats: state.firestore.ordered.flats,
  loading: state.async.loading
})

const FlatDashboard = ({flats, loading}) => {

  const [contextRef, setContextRef] = useState({});

  if(loading) return <LoadingComponent inverted={true} />

  return (
    <Container fluid className="FlatDashboard">
      <Row className="FlatDashboardRow">
      <Col xs={12} sm={12} md={8} lg={8} xl={8} className="p-0 px-2 mx-auto">
        <Container>
          <Row>
            <Col xs={12} sm={12} md={8} lg={8} xl={8} className="p-2">
              <div ref={setContextRef}>
                <FlatList flats={flats}/>
              </div>
            </Col>
            <Col xs={12} sm={12} md={4} lg={4} xl={4} className="p-2 d-none d-md-block">
              {/** This column won't be visible on small screens.
              It's content will be shown by selecting from the menu. */}
              <SideBar contextRef={contextRef}/>
            </Col>   
          </Row>
        </Container>
      </Col>
      </Row>
    </Container>
  )//return
}

export default connect(
  mapStateToProps
  )(firestoreConnect([{collection: 'flats'}])(FlatDashboard));
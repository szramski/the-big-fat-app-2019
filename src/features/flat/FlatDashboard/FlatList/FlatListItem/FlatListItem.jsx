import React from 'react';
import { Link } from 'react-router-dom';
import {
  Card,
  CardBody,
  CardHeader,
  CardImg,
  CardFooter,
  Container, Row, Col
} from 'reactstrap';
import { Icon, Button } from 'semantic-ui-react';


import './FlatListItem.scss';

const FlatListItem = ({ flat }) => {

  return (

    <Card className="mb-4 szr-card shadow">
      <CardHeader
        className="p-2 shadow-sm szr-flat-item-header text-truncate text-center h5">
        <span className="m-0">
          {flat.headline}
        </span>
      </CardHeader>
      <CardHeader className="p-2 text-truncate h5">
        <Container fluid>
          <Row>
            <Col xs={4} sm={4} md={4} className="p-0 text-left">
              <Icon name='bed' size="large" />{flat.numberOfBedrooms}
            </Col>
            <Col xs={4} sm={4} md={4} className="p-0 text-center">
              <Icon name='bath' size="large" />{flat.numberOfBathrooms}
            </Col>
            <Col xs={4} sm={4} md={4} className="p-0 text-right">
              <Icon name='tag' size="large" />
              {
                flat.rentCurrency === 'GBP' &&
                <span dangerouslySetInnerHTML={{ __html: '&#163;' }}></span>
              }
              {flat.rent}
              {
                flat.rentCurrency === 'PLN' &&
                <span dangerouslySetInnerHTML={{ __html: '&#122;&#322;' }}></span>
              }
              {
                flat.rentCurrency === 'EUR' &&
                <span dangerouslySetInnerHTML={{ __html: '&#8364;' }}></span>
              }
            </Col>
          </Row>
        </Container>
      </CardHeader>
      <CardBody className="p-0">
        {
          flat.flatImages.length !== 0 ?
            <CardImg
              top
              width="100%"
              src={flat.flatImages.filter(image => image.mainImage === true)[0].src}
              alt="Card image cap"
            />
            :
            <div className="szr-no-image">
              <span className="text-white display-4">
                No image available
            </span>
            </div>
        }
      </CardBody>
      <CardFooter className="p-2">
        <Container fluid>
          <Row>
            <Col xs={4} sm={4} md={4} className="p-0 pt-2 text-left h5">
              <Icon name='expand' size="large" />{flat.surface}&#x33A1;
            </Col>
            <Col xs={4} sm={4} md={4} className="p-0 pt-2 text-center h5">
              <Icon name='marker' size="large" /> {flat.address_details.city}
            </Col>
            <Col xs={4} sm={4} md={4} className="p-0 text-right">
              <Button as={Link} to={`/flat/${flat.id}`}>
                Details
              </Button>
            </Col>
          </Row>
        </Container>
      </CardFooter>
    </Card>
  )
}

export default FlatListItem;

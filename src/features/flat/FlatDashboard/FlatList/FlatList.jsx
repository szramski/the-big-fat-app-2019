import React from 'react';
import FlatListItem from './FlatListItem/FlatListItem';


const FlatList = ({ flats }) => {
  return (
    <div>
      {
        flats && flats.map(flat => <FlatListItem
          key={flat.id}
          flat={flat}
        />
        )
      }
    </div>
  )
}

export default FlatList;
import React from 'react';
import { Card, CardHeader, CardBody } from 'reactstrap';
import {Icon} from 'semantic-ui-react';

const FlatDetailedDescription = ({flat}) => (
  <Card className="my-4 szr-card">
    <CardHeader 
      className="p-2 szr-flat-item-header text-truncate text-center h5">
      <span> 
        <Icon name="info" />Description
      </span>
    </CardHeader>
  <CardBody className="p-2 text-justify">
    <p style={{whiteSpace: 'pre-line'}}>{flat.description}</p>
  </CardBody>
  </Card>
)
export default FlatDetailedDescription;

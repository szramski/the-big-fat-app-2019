import React, { useState } from 'react';
import { Card, CardHeader, CardBody, CardFooter, Collapse } from 'reactstrap';
import { Button } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';
import FlatCreatorInfo from './FlatCreatorInfo/FlatCreatorInfo';
import FlatDetailedImages from './FlatDetailedImages/FlatDetailedImages';
import FlatFacts from './FlatFacts/FlatFacts';

import Script from 'react-load-script';
import TheMap from '../../FlatForm/flatFormPages/TheMap';

const FlatDetailedHeader = ({ flat, history, currentUser }) => {

  const [showCreatorInfo, setShowCreatorInfo] = useState(false);
  const handleShowCreatorInfo = () => setShowCreatorInfo(!showCreatorInfo);

  const [showMap, setShowMap] = useState(false);
  const handleShowMap = () => setShowMap(!showMap);

  const [isGoogleApiScriptLoaded, setIsGoogleApiScriptLoaded] = useState(false);
  const handleScriptLoaded = () => setIsGoogleApiScriptLoaded(true);

  return (
    <Card className="my-2 szr-card">
       <Script 
        url="https://maps.googleapis.com/maps/api/js?key=AIzaSyDImUmR1lCEquHDAKssEn_7eEH2L2IRU80&libraries=places"
        onLoad={ handleScriptLoaded }
      />   
      <CardHeader
        className="p-2 shadow-sm szr-flat-item-header text-truncate text-center h5">
        <span className="m-0">
          {flat.headline}
        </span>
      </CardHeader>
      <CardBody className="p-0">
      {
          flat.flatImages && flat.flatImages.length !== 0 ?
            <FlatDetailedImages images={flat.flatImages} />
            :
            <div className="szr-no-image">
              <span className="text-white display-4">
                No image available
            </span>
            </div>
        }
      </CardBody>
      <CardFooter className="mt-5" style={{ backgroundColor: '#eee' }}>
        <Button
          //onClick={handleShowCreatorInfo} 
          color="red"
          icon="like"
          floated="left"
          className="mr-3"
        />
        <Button
          onClick={handleShowCreatorInfo}
          // content={ `${showCreatorInfo ? 'Hide': 'Show'} anouncer` }
          color="orange"
          icon="user"
          floated="left"
          className="mr-3"
        />
        <Button
          onClick={handleShowMap}
          // content={ `${showCreatorInfo ? 'Hide': 'Show'} map` }
          color="teal"
          icon="map"
          floated="left"
          className="mr-3"
        />
        <Button
          //onClick={handleShowCreatorInfo} 
          color="olive"
          icon="trash"
          floated="left"
        />
        <Button onClick={history.goBack} icon="arrow left" color="purple" floated="right" />
      </CardFooter>
      <Collapse isOpen={showMap}>
        <CardFooter className="p-0">
        {
        isGoogleApiScriptLoaded ?           
          <TheMap
            coords={flat.address_details.latlng}
            showLocationMarker={flat.publishStreetNumber}
            zoomLevel={ flat.publishStreetNumber ? 18 : 16 }
            containerElement={
              <div style={{
                height: '400px',
                boxShadow: '0px 0px 0px black'
              }} />
            }
            mapElement={<div style={{ height: `400px` }} />}
          />
          : <p>Loading Google Maps...</p>
          }
        </CardFooter>
      </Collapse>      
      <Collapse isOpen={showCreatorInfo}>
        <CardFooter>
          <FlatCreatorInfo flat={flat} currentUser={currentUser} />
        </CardFooter>
      </Collapse>
      <CardFooter>
        <FlatFacts flat={flat} />
      </CardFooter>
    </Card>
  )
}

export default withRouter(FlatDetailedHeader)

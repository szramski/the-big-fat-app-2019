import React from 'react';
import {Container, Row, Col} from 'reactstrap';
import moment from 'moment';

const FlatFacts = ({flat}) => {
  return (
    <Container fluid>
      <Row className="border-bottom text-center">
        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
          <div className="h1 m-0">
          { flat.rentCurrency === 'GBP' && <span dangerouslySetInnerHTML={{__html: '&#163;'}}></span>}
          {flat.rent}
          { flat.rentCurrency === 'PLN' && <span dangerouslySetInnerHTML={{__html: '&#122;&#322;'}}></span>  }
          { flat.rentCurrency === 'EUR' && <span dangerouslySetInnerHTML={{__html: '&#8364;'}}></span>  }
          <small>{` per ${flat.paymentPeriod}`}</small>
          </div>
        </Col>      
      </Row>          
      <Row className="py-2 text-center">
        <Col xs={6} sm={6} md={6} lg={6} xl={6}>
          {
            //`${flat.flatLikes.length} ${flat.flatLikes.length === 1 ? 'Like' : 'Likes'}`
          }
        </Col>  
        <Col xs={6} sm={6} md={6} lg={6} xl={6}>
          {
            //`${flat.flatViewed.length} ${flat.flatViewed.length === 1 ? 'View' : 'Views'}` 
          }
        </Col>                           
      </Row> 
      <Row className="py-2 text-left">
      <Col xs={12} sm={12} md={4} lg={4} xl={4}>
          Reference: {flat.referenceId !== '' ? flat.referenceId : '-'}
        </Col>        
      <Col xs={12} sm={12} md={4} lg={4} xl={4}>
          Available from: {moment(flat.availableFrom).format('LL')}
        </Col>         
        <Col xs={12} sm={12} md={4} lg={4} xl={4}>
          Deposit: {flat.deposit}
        </Col>             
      </Row>    

      <Row className="py-2 text-left">
        <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          City: {flat.address_details.city}
        </Col>  
        <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          Type: {flat.propertyType}
        </Col>  
        <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          Surface: {flat.surface} &#x33A1;
        </Col>    
        <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          Usable: {flat.surfaceNet} &#x33A1;
        </Col>                                 
      </Row>  

      <Row className="py-2 text-left">
      <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          Bedrooms: {flat.numberOfBedrooms}
        </Col>       
        <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          Bathrooms: {flat.numberOfBathrooms}
        </Col>  
        <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          Floor: {flat.floor} of {flat.floorTotal}
        </Col>    
        <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          Elevator: {flat.ftrElevator ? 'Yes' : 'No'}
        </Col>                                  
      </Row> 
    </Container>
  )
}

export default FlatFacts;
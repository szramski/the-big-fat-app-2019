import React from 'react';
import UserAvatar from 'react-user-avatar';
import {Button} from 'semantic-ui-react';
import {Link} from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';

import './FileCreatorInfo.css';

const FlatCreatorInfo = ({flat, currentUser}) => {
    return (
      <Container 
      className="text-truncate"
      style={{
        height: '64px',
      }}>
        <Row>
          <Col xs={2} sm={2} md={2} lg={2} xl={2} className="mx-auto p-0">
            <UserAvatar 
              size="64" 
              name={flat.creatorDisplayName} 
              src={flat.creatorPhotoURL} 
            />   
          </Col>
          <Col xs={10} sm={10} md={10} lg={10} xl={10}>
            <p className="szr-file-creator m-0">
            Posted by {flat.creatorDisplayName}
            </p>
            <p>
            {
              //Only if the currently logged-in user is not the creator of the
              //flat, we display the message button.

              flat.creatorUid !== currentUser.uid &&
              <Button 
                as={Link}
                to={`/message/${flat.creatorUid}/${currentUser.uid}`}              
                icon="envelope"
                color="violet"
                content="Message"
                size="small"
                className="mr-3"
                />
            }
              <Button 
                as={Link}
                to={`/profile/${flat.creatorUid}`}
                icon="user"
                color="red"
                content="Profile"
                size="small"
                />                
            </p>         
          </Col>          
        </Row>
      </Container>
    )//return
}

export default FlatCreatorInfo;

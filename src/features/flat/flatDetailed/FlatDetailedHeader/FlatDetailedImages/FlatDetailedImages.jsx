import React from 'react';

import Slick from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import './FlatDetailedImages.scss';

const slickSettings = {
  dots: true,
  dotsClass: "slick-dots slick-thumb",
  infinite: true,
  arrows: true,
  speed: 1000,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay:true,
  fade: true,
};

const FlatDetailedImages = ({images}) => {
  return (
    <Slick {...slickSettings} className="slick">
      {
        images.map( (image, index) => (
          <div key={index} className="featured-item">
            <div className="featured_image"
              style={{background: `url(${image.src})`}}>
            </div>              
          </div>
        ))
      }
    </Slick>
  )
}

export default FlatDetailedImages;

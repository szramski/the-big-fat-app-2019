import React from 'react';
import { Card, CardHeader, CardBody } from 'reactstrap';
import {Icon} from 'semantic-ui-react';

const FlatDetailedComments = () => {
  return (
    <Card className="my-4 szr-card">
      <CardHeader 
        className="p-2 szr-flat-item-header text-truncate text-center h5">
        <span>
          <Icon name="comment" /> Comments
        </span>
      </CardHeader>
    <CardBody className="p-2 text-justify">
      Comment Section
    </CardBody>
    </Card>
  )
}

export default FlatDetailedComments

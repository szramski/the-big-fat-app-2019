import React, {Component} from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
  withFirestore, 
  //firebaseConnect
} from 'react-redux-firebase';
import {toastr} from 'react-redux-toastr';

import { Container, Row, Col } from 'reactstrap';

import FlatDetailedHeader from '../FlatDetailedHeader/FlatDetailedHeader';
import FlatDetailedDescription from '../FlatDetailedDescription/FlatDetailedDescription';
import FlatDetailedComments from '../FlatDetailedComments/FlatDetailedComments';
import FlatDetailedFacts from '../FlatDetailedFacts/FlatDetailedFacts';

import LoadingComponent from '../../../../app/layout/LoadingComponent';

import './FlatDetailedPage.scss';

const mapStateToProps = state => {

  let currFlat = {}; //initialize an empty flat object

  if(state.firestore.ordered.flats && state.firestore.ordered.flats[0]) {
    //see the comments in useEffect. We're expecting to have the reducer populated
    //with the data of the requested flat
    currFlat = state.firestore.ordered.flats[0];
  }

  return { 
    currFlat,
    currentUser: {...state.firebase.profile, uid: state.firebase.auth.uid },
    fstoreInRequestingStatus: state.firestore.status.requesting //to prevent errors with undefined variables
  };
} //mapStateToProps 

const actions = { 
  //addFlatComment, openModal 
}

class FlatDetailedPage extends Component {

state = {initialComponentLoading: true};

async componentDidMount(){

  const {firestore, match } = this.props;

    //When the component mounts, we look for the document of which the id
    //corresponds with the id provided in the URL params:
    let flat = await firestore.get(`/flats/${match.params.id}`);

    //This gives us a document snapshot and it also populates our array in
    //state.firestore.ordered.flats with only the one flat that we're looking for.

    //We take advantage of the "exists" property of the document snapshot in order
    //to redirect the user back in case that the id provided in URL does not match
    //any existing snapshots

  if(!flat.exists){
    //If the event doesn't exist:
    toastr.error('Not Found', 'Requested Event does not exist');
    this.props.history.push('/feed');
  } else {
  //If the event does exist:
  //await firestore.setListener(`events/${match.params.id}`);
  }

  this.setState({initialComponentLoading: false})

}//componentDidMount

  render(){

    const { 
      match, 
      currFlat, 
      currentUser,
      fstoreInRequestingStatus,
      } = this.props;

      console.log("currenttttt", currFlat)

      const fstoreRequestingCurrFlat = fstoreInRequestingStatus[`flats/${match.params.id}`];

      if(this.state.initialComponentLoading || fstoreRequestingCurrFlat) 
        return <LoadingComponent inverted={true} />

    return(
      <Container className="FlatDetailedPage" fluid>
      <Row className="FlatDetailedPageRow">
        <Col xs={12} sm={12} md={8} lg={8} xl={8} className="p-0 px-2 mx-auto">
          <FlatDetailedHeader flat={currFlat} currentUser={currentUser} />
          <FlatDetailedDescription flat={currFlat} />
          <FlatDetailedFacts flat={currFlat} />  
          <FlatDetailedComments />
        </Col>
      </Row>
    </Container>
    )//return
  }//render
}//class

export default compose(
  withFirestore,
  connect(mapStateToProps, actions), 
  //firebaseConnect( props => ([`flat_chat/${props.match.params.id}`]) )
)(FlatDetailedPage);

import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Card, CardHeader, CardBody } from 'reactstrap';
import { Icon } from 'semantic-ui-react';

const FurtherFacts = ({flat}) => {

  const paintYesOrNo = value => value ?
    <span className="text-success px-1">
      <Icon name="check" />
    </span>
    :
    <span className="text-danger px-1">
      <Icon name="ban" />
    </span>

  return (
    <Card className="my-4 szr-card">
      <CardHeader
        className="p-2 szr-flat-item-header text-truncate text-center h5">
        <span>
          <Icon name="list" />Facts
      </span>
      </CardHeader>
      <CardBody className="p-2 text-justify">
        <Container fluid>
          <Row className="text-left p-0 mt-0 mb-3 border-bottom">
            <Col xs={12} sm={12} md={12} lg={12} xl={12} className="p-0">
              <h4 className="m-0 mb-1 p-0">General Info</h4>
            </Col>
          </Row>
          {
            flat.additionalCosts > 0 &&
            <Row className="text-left p-0 my-2">
              <Col xs={12} sm={12} md={12} lg={12} xl={12} className="p-0">
                Additional Costs:{' '}
                {flat.rentCurrency === 'GBP' && <span dangerouslySetInnerHTML={{ __html: '&#163;' }}></span>}
                {flat.additionalCosts}
                {flat.rentCurrency === 'PLN' && <span dangerouslySetInnerHTML={{ __html: '&#122;&#322;' }}></span>}
                {flat.rentCurrency === 'EUR' && <span dangerouslySetInnerHTML={{ __html: '&#8364;' }}></span>}
                {' '}per {flat.additionalCostsPaymentPeriod}
              </Col>
            </Row>
          }
          <Row className="text-left p-0 my-2">
            <Col xs={12} sm={12} md={4} lg={4} xl={4} className="p-0">
              Construction Year: {flat.yearConstruction}
            </Col>
            <Col xs={12} sm={12} md={4} lg={4} xl={4} className="p-0">
              Solar orientation: {flat.solarOrientation}
            </Col>
            <Col xs={12} sm={12} md={4} lg={4} xl={4} className="p-0">
              EPC Rating: {flat.epcRatingType}
            </Col>
          </Row>
          <Row className="text-left p-0 my-2">
            <Col xs={12} sm={12} md={4} lg={4} xl={4} className="p-0">
              Most recent renovation: {flat.yearRenovation}
            </Col>
            <Col xs={12} sm={12} md={4} lg={4} xl={4} className="p-0">
              Total number of floors: {flat.floorTotal}
            </Col>
            <Col xs={12} sm={12} md={4} lg={4} xl={4} className="p-0">
              Furnishing: {flat.furnishingType}
            </Col>
          </Row>

          <Row className="text-left p-0 my-3 border-bottom">
            <Col xs={12} sm={12} md={12} lg={12} xl={12} className="p-0">
              <h4 className="m-0">Tenant Preference</h4>
            </Col>
          </Row>

          <Row className="text-left p-0 my-2">
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Somkers allowed: {paintYesOrNo(flat.prefSmokersAllowed)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Pets allowed: {paintYesOrNo(flat.prefPetsAllowed)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Students allowed: {paintYesOrNo(flat.prefStudentsAllowed)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">

            </Col>
          </Row>

          <Row className="text-left p-0 my-3 border-bottom">
            <Col xs={12} sm={12} md={12} lg={12} xl={12} className="p-0">
              <h4 className="m-0">Features</h4>
            </Col>
          </Row>

          <Row className="text-left p-0 my-2">
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Balcony: {paintYesOrNo(flat.ftrBalcony)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Air Con: {paintYesOrNo(flat.ftrAirCon)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Heating: {paintYesOrNo(flat.ftrHeating)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Built-In Closets: {paintYesOrNo(flat.ftrBuiltInClosets)}
            </Col>
          </Row>

          <Row className="text-left p-0 my-2">
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Reinforced Door: {paintYesOrNo(flat.ftrReinforcedDoor)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Parquet Flooring: {paintYesOrNo(flat.ftrParquetFlooring)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Fridge: {paintYesOrNo(flat.ftrFridge)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Stove: {paintYesOrNo(flat.ftrStove)}
            </Col>
          </Row>

          <Row className="text-left p-0 my-2">
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Microwave: {paintYesOrNo(flat.ftrMicrowave)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Washing Machine: {paintYesOrNo(flat.ftrWashingMachine)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Dishwasher: {paintYesOrNo(flat.ftrDishwasher)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              TV: {paintYesOrNo(flat.ftrTV)}
            </Col>
          </Row>


          <Row className="text-left p-0 my-3 border-bottom">
            <Col xs={12} sm={12} md={12} lg={12} xl={12} className="p-0">
              <h4 className="m-0">Other Features</h4>
            </Col>
          </Row>


          <Row className="text-left p-0 my-2">
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Garage: {paintYesOrNo(flat.ftrGarage)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Basement {paintYesOrNo(flat.ftrBasement)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Pool: {paintYesOrNo(flat.ftrPool)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">

            </Col>
          </Row>

          <Row className="text-left p-0 my-2">
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Garden: {paintYesOrNo(flat.ftrGarden)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Playground: {paintYesOrNo(flat.ftrPlayground)}
            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">

            </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">

            </Col>
          </Row>

          <Row className="text-left p-0 my-3 border-bottom">
            <Col xs={12} sm={12} md={12} lg={12} xl={12} className="p-0">
              <h4 className="m-0">Situation</h4>
            </Col>
          </Row>

          <Row className="text-left p-0 my-2">
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Country: {flat.address_details.country}
        </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Region: {flat.address_details.region}
        </Col>
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              City: {flat.address_details.city}
        </Col>
            {flat.publishStreetNumber
              ?
              <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
                Postal code: {flat.address_details.postalcode}
              </Col>
              : null
            }
          </Row>
          <Row className="text-left p-0 my-2">
          {flat.publishStreetNumber
              ?
              <Col xs={12} sm={12} md={6} lg={6} xl={6} className="p-0">
                Street Name: {flat.address_details.streetName}
            </Col>
              : null
            }

            {flat.publishStreetNumber
              ?
              <Col xs={12} sm={12} md={6} lg={6} xl={6} className="p-0">
                Street Number: {flat.address_details.streetNumber}
            </Col>
              : null
            }

          </Row>
        </Container>
      </CardBody>
    </Card>
  )
}

export default FurtherFacts;
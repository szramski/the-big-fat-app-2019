import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import cuid from 'cuid';

//==== Image manipulation ========================================
import Dropzone from 'react-dropzone';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';

//==== Design ====================================================
import { Container, Row, Col } from 'reactstrap';
import { Form, Icon, Image, Button, Divider } from 'semantic-ui-react';

const mapState = state => {

  const maxAllowedImages = 10;

  return {
    maxAllowedImages,
    flatImages: state.form.flatForm.values.flatImages
  }

}

const mapActions = {

};

class Pictures extends Component {

  state = {
    files: [],
    fileName: '',
    cropResult: null,
    croppedImage: {},
  }

  onDropFiles = droppedFiles => {
    //we allow to drop one file each time but even one file
    //will come in within an array.

    this.setState({
      files: droppedFiles,
      fileName: droppedFiles[0].name
    })

  }


  cropImage = () => {
    if (typeof this.refs.cropper.getCroppedCanvas() === undefined) return;

    this.refs.cropper.getCroppedCanvas().toBlob(blob => {
      let imageURL = URL.createObjectURL(blob);

      this.setState({
        cropResult: imageURL,
        croppedImage: blob
      })

    }, 'image/jpeg'); //toBlob
  }

  addImage = () => {

    let prevImages = this.props.flatImages;
    let newImages = prevImages.concat({
      tmpId: cuid(),
      src: this.state.cropResult,
      blob: this.state.croppedImage,
      mainImage: prevImages.length === 0 ? true : false, //If we're adding the first image, then we set it to be the mainImage
    });

    this.setState({
      cropResult: null,
      croppedImage: {},
      files: [],
      fileName: '',
    });

    this.props.change('flatImages', newImages);
  }

  removeImage = id => () => {

    let updatedImages = this.props.flatImages.filter(img => img.tmpId !== id);

    this.props.change('flatImages', updatedImages);

  }


  setMainImage = id => () => {

    //We want to take the image with tempId === id and set it's mainImage property to true
    //So we take the original flatImages array and map through it. If the id of the image that
    //we currently are looking at equals the one that we want to change, then we return a new
    //object that takes over the previous properties but has "true" as value for the mainImage property.
    //Otherwise we return "false" as the mainImage property. Thus we return a new array that we assign
    //to the flatImages array in setState:

    const updatedImages = this.props.flatImages.map(img => {
      if (img.tmpId === id) {
        return Object.assign({}, {
          tmpId: img.tmpId,
          src: img.src,
          mainImage: true
        })
      } else {
        return Object.assign({}, {
          tmpId: img.tmpId,
          src: img.src,
          mainImage: false
        })
      }
    })//map

    this.props.change('flatImages', updatedImages);
  }


  render() {

    const { maxAllowedImages, flatImages } = this.props;

    return (
      <Form>
        <Container fluid>
          {
            maxAllowedImages &&
            (maxAllowedImages - flatImages.length > 0)
            &&
            //only if the user hasn't added the maximum number of fotos,
            //we show the row that allows to upload fotos
            <Row className="">
              <Col xs={12} sm={12} md={4} xl={4} className="p-1" >
                <h5>Step 1 - Select photo</h5>
                <Dropzone
                  accept="image/jpeg, image/png"
                  className="p-0 pt-5 text-center border border-info"
                  onDrop={this.onDropFiles}
                  multiple={false}
                  style={{
                    minWidth: '99%',
                    width: '99%',
                    minHeight: '150px',
                    height: '150px',
                    backgroundColor: '#ccc',
                    cursor: 'pointer',
                  }}
                >
                  <div>
                    <Icon name="upload" size="big" />
                    <h5>Drop image here or click to select</h5>
                  </div>
                </Dropzone>
              </Col>
              {this.state.files[0] &&
                <Col xs={12} sm={12} md={4} xl={4} className="p-1 mt-3 mt-md-0">
                  <div>
                    <h5>Step 2 - Resize photo</h5>
                    <Cropper
                      style={{
                        minWidth: '99%',
                        width: '99%',
                        minHeight: '150px',
                        height: '150px',
                        backgroundColor: '#ccc',
                        cursor: 'pointer',
                      }}
                      ref='cropper'
                      src={this.state.files[0].preview}
                      aspectRactio={1} //square photos
                      viewMode={0}
                      dragMode='move'
                      guides={true}
                      scalable={true}
                      cropBoxMovable={true}
                      cropBoxResizable={true}
                      crop={this.cropImage}
                    />
                  </div>
                </Col>
              }
              {
                this.state.files[0] &&
                <Col xs={12} sm={12} md={4} xl={4} className="p-1 mt-3 mt-md-0">

                  <div>
                    <h5>Step 3 - Preview and add</h5>
                    <Image
                      src={this.state.cropResult}
                      className="border border-info"
                      style={{
                        minHeight: '150px',
                        height: '150px',
                        minWidth: '99%',
                        width: '99%'
                      }}
                    />
                    <Button basic color='black'
                      content='Cancel'
                      className="float-left mt-1"
                    />
                    <Button
                      basic color='green'
                      content='Add'
                      className="float-right mt-1"
                      onClick={this.addImage}
                    />
                  </div>
                </Col>
              }
            </Row>
          }
          {
            flatImages && flatImages.length > 0 &&
            <Row>
              <Col xs={12} sm={12} md={12} xl={12} className="p-1 mt-3 mt-md-0 text-center">
                <Divider horizontal>
                  Added photos
                </Divider>
                {
                  (maxAllowedImages - flatImages.length > 0) &&
                  `You have added ${flatImages.length} of ${maxAllowedImages} photos.`
                }
              </Col>
            </Row>
          }
          {
            flatImages && flatImages.length > 0 &&
            <Row>
              {
                flatImages.map(image => (
                  <Col key={image.tmpId} xs={12} sm={12} md={6} xl={6} className="p-1 mt-3 mt-md-0">
                    <Image
                      src={image.src}
                      className="border border-info"
                      style={{
                        // minWidth: '220px',
                        // width: '220px',
                        // minHeight: '140px',
                        // height: '140px',

                      }}
                    />
                    {
                      !image.mainImage ?
                        <Button basic color='purple' size='mini'
                          content='Set main photo'
                          className="float-left mt-1"
                          onClick={this.setMainImage(image.tmpId)}
                        />
                        :
                        <p className="float-left mt-1">Main photo</p>
                    }
                    <Button
                      basic color='orange' size='mini'
                      content='Remove'
                      className="float-right mt-1"
                      onClick={this.removeImage(image.tmpId)}
                    />
                  </Col>
                ))
              }
            </Row>
          }
        </Container>
      </Form>
    )//return

  }//render
}

export default reduxForm({
  form: 'flatForm',
  destroyOnUnmount: false,
  //enableReinitialize: true,
  forceUnregisterOnUnmount: true,
})(
  connect(mapState, mapActions)(Pictures)
);
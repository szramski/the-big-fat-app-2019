import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';


//==== Design ====================================================
import { Container, Row, Col } from 'reactstrap';
import { Form, Divider } from 'semantic-ui-react';

//==== Form Components ============================================
import CheckBox from '../../../../app/util/formFields/CheckBox';

import validate from '../validate';

const mapState = state => ({

})

const mapActions = {

};

const Features = ({ change }) => {
  return (
    <Form>
      <Container fluid>
        <Row>
        <Col xs={12} sm={12} md={12} xl={12} className="p-0">
          <Divider horizontal>Tenant preference</Divider>   
        </Col>       
        </Row>
        <Row>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="prefStudentsAllowed"
            checkBoxText="Students Allowed"
            component={CheckBox}
          />             
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="prefPetsAllowed"
            checkBoxText="Pets Allowed"
            component={CheckBox}
          />             
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="prefSmokersAllowed"
            checkBoxText="Smokers Allowed"
            component={CheckBox}
          />             
          </Col>                    
        </Row>

        {/* ============================== */}
        {/* ============================== */}
        <Row>
        <Col xs={12} sm={12} md={12} xl={12} className="p-0">
          <Divider horizontal>Property features</Divider>   
        </Col>       
        </Row>        
        <Row>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrBalcony"
            checkBoxText="Balcony"
            component={CheckBox}
          />             
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrAirConditioning"
            checkBoxText="Air Conditioning"
            component={CheckBox}
          />             
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrHeating"
            checkBoxText="Heating"
            component={CheckBox}
          />             
          </Col>                    
        </Row>
        <Row>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrBuiltInClosets"
            checkBoxText="Built-in Closets"
            component={CheckBox}
          />             
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrReinforcedDoor"
            checkBoxText="Reinforced Door"
            component={CheckBox}
          />             
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrParquetFlooring"
            checkBoxText="Parquet Flooring"
            component={CheckBox}
          />             
          </Col>                    
        </Row>
        <Row>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrFridge"
            checkBoxText="Fridge"
            component={CheckBox}
          />             
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrStove"
            checkBoxText="Stove"
            component={CheckBox}
          />             
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrMicrowave"
            checkBoxText="Microwave"
            component={CheckBox}
          />             
          </Col>                    
        </Row>
        <Row>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrWashingMachine"
            checkBoxText="Washing Machine"
            component={CheckBox}
          />             
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrDishwasher"
            checkBoxText="Dishwasher"
            component={CheckBox}
          />             
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrTV"
            checkBoxText="TV"
            component={CheckBox}
          />             
          </Col>                    
        </Row>                        

        {/* ============================== */}
        {/* ============================== */}
        <Row>
        <Col xs={12} sm={12} md={12} xl={12} className="p-0">
          <Divider horizontal>Other features</Divider>   
        </Col>       
        </Row>
        <Row>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrGarage"
            checkBoxText="Garage"
            component={CheckBox}
          />             
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrBasement"
            checkBoxText="Basement"
            component={CheckBox}
          />             
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrElevator"
            checkBoxText="Elevator"
            component={CheckBox}
          />             
          </Col>                    
        </Row>
        <Row>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrPool"
            checkBoxText="Pool"
            component={CheckBox}
          />             
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrGarden"
            checkBoxText="Garden"
            component={CheckBox}
          />             
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="p-0">
           <Field 
            name="ftrPlayground"
            checkBoxText="Playground"
            component={CheckBox}
          />             
          </Col>  
        </Row>
      </Container>
    </Form>
  )
}

export default reduxForm({
  form: 'flatForm',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate
})(
  connect(mapState, mapActions)(Features)
);

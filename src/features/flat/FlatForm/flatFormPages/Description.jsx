import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';

//==== Design ====================================================
import { Container, Row, Col } from 'reactstrap';
import { Form } from 'semantic-ui-react';

//==== Form Components ============================================
import TextInput from '../../../../app/util/formFields/TextInput';
import TextArea from '../../../../app/util/formFields/TextArea';

import validate from '../validate';

const mapState = state => ({

})

const mapActions = {

};

const Description = ({handleSubmit}) => {
  return (
    <Form>            
    <Container fluid>  
      <Row>
        <Col className="p-0">
          <Field 
            name="headline"
            label="Headline"
            type="text"
            component={TextInput}
            placeholder="Enter a headline for the announcement."
          />  
        </Col>
      </Row>
      <Row className="mt-3">
        <Col className="p-0">
          <Field 
            name="description"
            label="Description (optional)"
            type="text"
            component={TextArea}
            rows={5}
            placeholder="Describe the property in as many lines and paragraphs as you wish."
          />                    
        </Col>
      </Row>
      <Row className="mt-3">
        <Col className="p-0">
          <Field 
            name="referenceId"
            label="Reference ID (optional)"
            type="text"
            component={TextInput}
            placeholder="Optional reference number of the announcement."
          />  
        </Col>
      </Row> 
    </Container>
    </Form>
  )
}

export default reduxForm({
  form: 'flatForm',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate
})(
  connect(mapState, mapActions)(withRouter(Description))
);

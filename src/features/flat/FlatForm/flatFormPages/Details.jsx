import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';


//==== Design ====================================================
import { Container, Row, Col } from 'reactstrap';
import { Form } from 'semantic-ui-react';

//==== Form Components ============================================
import SelectInput from '../../../../app/util/formFields/SelectInput';
import DateInput from '../../../../app/util/formFields/DateInput';
import TextInput from '../../../../app/util/formFields/TextInput';
import NumInput from '../../../../app/util/formFields/NumInput';
import CheckBox from '../../../../app/util/formFields/CheckBox';

import {
  typesOfProperty,
  typesOfEpcRating,
  typesOfSolarOrientation,
  typesOfFurnishing,
} from '../../../../app/util/formDataLists';

import validate from '../validate';

const mapState = state => ({

})

const mapActions = {

};

const Details = ({ change }) => {
  return (
    <Form>
      <Container fluid>
        <Row>
          <Col xs={12} sm={12} md={4} xl={4} className="py-0 px-1">
                <Field
                  name="publishStreetNumber"
                  label="Publish street, street number and ZIP code?"
                  checkBoxText="Publish sensitive data"
                  component={CheckBox}
                  showborder={true}
                />
            </Col>   
          <Col xs={12} sm={12} md={4} xl={4} className="py-0 px-1">
            <Field
              name="propertyType"
              label="Property Type"
              type="text"
              component={SelectInput}
              options={typesOfProperty}
              placeholder="For example: Flat"
            />
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="py-0 px-1">
            <Field
              name="availableFrom"
              label="Available from"
              component={DateInput}
              change={change}
            />
          </Col>
        </Row>

        {/* ============================== */}
        {/* ============================== */}

        <Row className="mt-3">
          <Col xs={12} sm={12} md={4} xl={4} className="py-0 px-1">
            <Field
              name="furnishingType"
              label="Furnishing"
              type="text"
              component={SelectInput}
              options={typesOfFurnishing}
              placeholder=""
            />
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="py-0 px-1">
            <Field
              name="solarOrientation"
              label="Solar orientation"
              type="text"
              component={SelectInput}
              options={typesOfSolarOrientation}
              placeholder=""
            />
          </Col> 
          <Col xs={12} sm={12} md={4} xl={4} className="py-0 px-1">
            <Field
              name="epcRatingType"
              label="EPC Rating"
              type="text"
              component={SelectInput}
              options={typesOfEpcRating}
              placeholder=""
            />
          </Col>
          </Row>
          <Row className="mt-3">               
          <Col xs={12} sm={12} md={3} xl={3} className="py-0 px-1">
            <Field
              name="numberOfBedrooms"
              label="Number of bedrooms"
              component={NumInput}
              min={1}
              max={100}
            />
          </Col>
          <Col xs={12} sm={12} md={3} xl={3} className="py-0 px-1">
            <Field
              name="numberOfBathrooms"
              label="Number of bathrooms"
              component={NumInput}
              min={1}
              max={100}
            />
          </Col>
          <Col xs={12} sm={12} md={3} xl={3} className="py-0 px-1">
            <Field
              name="floor"
              label="Floor"
              component={NumInput}
              min={1}
              max={100}
            />
          </Col>
          <Col xs={12} sm={12} md={3} xl={3} className="py-0 px-1">
            <Field
              name="floorTotal"
              label="Total floor count"
              component={NumInput}
              min={1}
              max={100}
            />            
          </Col>
        </Row>


        {/* ============================== */}
        {/* ============================== */}

        <Row className="mt-3">
          <Col xs={12} sm={12} md={3} xl={3} className="py-0 px-1">
            <Field
              name="surface"
              label="Surface (&#13217;)"
              component={NumInput}
              min={1}
              max={1000}
            />               
          </Col>
          <Col xs={12} sm={12} md={3} xl={3} className="py-0 px-1">
            <Field
              name="surfaceNet"
              label="Net surface (&#13217;)"
              component={NumInput}
              min={1}
              max={1000}
            />              
          </Col>
          <Col xs={12} sm={12} md={3} xl={3} className="py-0 px-1">
            <Field
              name="yearConstruction"
              label="Year of construction"
              type="text"
              component={TextInput}
              placeholder=""
            />
          </Col>
          <Col xs={12} sm={12} md={3} xl={3} className="py-0 px-1">
            <Field
              name="yearRenovation"
              label="Year of last renovation"
              type="text"
              component={TextInput}
              placeholder=""
            />
          </Col>
        </Row>
      </Container>
    </Form>
  )
}

export default reduxForm({
  form: 'flatForm',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate
})(
  connect(mapState, mapActions)(Details)
);

import React from 'react';
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps';

const RenderMap = ({ coords, showLocationMarker, zoomLevel }) => {

return (
  <GoogleMap
  defaultZoom={zoomLevel}
  className="border shadow-sm mb-3"
  center={{ lat: coords.lat, lng: coords.lng }}>
  {
    showLocationMarker && <Marker position={{ lat: coords.lat, lng: coords.lng }} />
  }
</GoogleMap>

)
}

export default withGoogleMap(RenderMap);
import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';

//==== Design ====================================================
import { Container, Row, Col } from 'reactstrap';
import { Form } from 'semantic-ui-react';

//==== Form Components ============================================
import PlaceField from '../../../../app/util/formFields/PlaceField';
import TheMap from './TheMap';

import { getLatLng, geocodeByAddress } from "react-places-autocomplete";
import getLocationDetails from "./getAddressDetails";

const mapState = state => ({
  coordsForGoogleMap: state.form.flatForm.values.address_details.latlng,
  addressStringForMap: state.form.flatForm.values.address_details.formattedAddress,
})

const mapActions = {

};

const Location = ({ coordsForGoogleMap, addressStringForMap, change }) => {

  const onAddressSelect = async selectedAddress => {

    let geocodeResult;
    let latlng;

    try {
      geocodeResult = await geocodeByAddress(selectedAddress);

      latlng = await getLatLng(geocodeResult[0]);

    } catch (error) {
      console.log("Error with geocodeByAddress", error)
    }

    let address_details = getLocationDetails(geocodeResult[0]);
    address_details.latlng = latlng;

    change('address_details', address_details);
    change('addressSearchString', address_details.formattedAddress);
  }


  return (
    <Form>
      <Container fluid>
        <Row>
          <Col xs={12} sm={12} md={12} xl={12} className="p-0">
            <Field
              name="addressSearchString" //must stay in place, also for validation!
              label="Address lookup"
              placeholder="Enter search term here"
              component={PlaceField}
              options={{
                componentRestrictions: { country: ['de', 'es', 'pl', 'gb'] },
              }}
              onSelect={onAddressSelect}
            />
          </Col>
        </Row>
        {
          coordsForGoogleMap.lat !== 0 && coordsForGoogleMap.lng !== 0 &&
          <div>
            <Row>
              <Col xs={12} sm={12} md={12} xl={12} className="p-0 mt-3">
                <TheMap
                  coords={coordsForGoogleMap}
                  zoomLevel={18}                  
                  containerElement={
                    <div style={{
                      height: '400px',
                      boxShadow: '0px 0px 1px black'
                    }} />
                  }
                  mapElement={<div style={{ height: `400px` }} />}
                />
              </Col>
            </Row>
          </div>
        }
      </Container>
    </Form>
  )
}

export default reduxForm({
  form: 'flatForm',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
})(
  connect(mapState, mapActions)(Location)
);


import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';


//==== Design ====================================================
import { Container, Row, Col } from 'reactstrap';
import { Form } from 'reactstrap';

//==== Form Components ============================================
import SelectInput from '../../../../app/util/formFields/SelectInput';
import NumInput from '../../../../app/util/formFields/NumInput';

import {
  typesOfCurrency,
  typesOfDeposit,
  typesOfPayment,
} from '../../../../app/util/formDataLists';

import validate from '../validate';

const mapState = state => ({

})

const mapActions = {

};

const Payment = ({ change }) => {
  return (
    <Form>            
    <Container fluid>  
      <Row>
          <Col xs={12} sm={12} md={4} xl={4} className="py-0 px-1">
          <Field
              name="rent"
              label="Rent"
              component={NumInput}
              min={1}
              max={10000}
            />         
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="py-0 px-1">
          <Field
              name="rentCurrency"
              label="Currency"
              type="text"
              component={SelectInput}
              options={typesOfCurrency}
              placeholder=""
            />
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="py-0 px-1">
          <Field
              name="paymentPeriod"
              label="Paid per"
              type="text"
              component={SelectInput}
              options={typesOfPayment}
              placeholder=""
            />
          </Col>          
          </Row>

        {/* ============================== */}
        {/* ============================== */}

          <Row className="mt-3">
          <Col xs={12} sm={12} md={4} xl={4} className="py-0 px-1">
          <Field
              name="deposit"
              label="Deposit"
              type="text"
              component={SelectInput}
              options={typesOfDeposit}
              placeholder=""
            />
          </Col>     
          <Col xs={12} sm={12} md={4} xl={4} className="py-0 px-1">
            <Field
              name="additionalCosts"
              label="Additional costs"
              component={NumInput}
              min={1}
              max={10000}
            />            
          </Col>
          <Col xs={12} sm={12} md={4} xl={4} className="py-0 px-1">
          <Field
              name="additionalCostsPaymentPeriod"
              label="Additional costs paid per"
              type="text"
              component={SelectInput}
              options={typesOfPayment}
              placeholder=""
            />
          </Col>  
        </Row>
      </Container>
    </Form>
  )
}

export default reduxForm({
  form: 'flatForm',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate
})(
  connect(mapState, mapActions)(Payment)
);

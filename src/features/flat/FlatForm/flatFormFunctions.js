import cuid from 'cuid';
import {toastr} from 'react-redux-toastr';

import {getFlatArrayFromLocalStorage, setFlatArrayInLocalStorage } from '../../../app/util/utilFunctions';
import moment from 'moment';

export const createDraftFlatAndStoreInLocalStorage = flat => {

  //Change the availableFrom date from a moment object to an ISO date string:
  //for example to: "2019-01-05T17:36:10+01:00"
  flat.availableFrom = moment(flat.availableFrom).format();

  //add a draftId to the values object
  flat.draftId = cuid();

  let flatDraftArray = getFlatArrayFromLocalStorage();

  //add the current draft to the array
  flatDraftArray.push(flat);

  //write the array to localstorage
  setFlatArrayInLocalStorage(flatDraftArray);

  toastr.success('Created draft', `Draft "${flat.headline}" has been created.`);

  return flat.draftId;
} //createDraftFlatAndStoreInLocalStorage

//================================
//================================

export const updateDraftFlatInLocalStorage = flat => {

  //Change the availableFrom date from a moment object to an ISO date string:
  //for example to: "2019-01-05T17:36:10+01:00"
  flat.availableFrom = moment(flat.availableFrom).format();

  let flatDraftArray = getFlatArrayFromLocalStorage();

  //remove the original flat from the flatDraftArray
  flatDraftArray = flatDraftArray.filter( item => item.draftId !== flat.draftId );

  //add the updated flat into the Array:
  flatDraftArray.push(flat);

  //write the array to localstorage
  setFlatArrayInLocalStorage(flatDraftArray);

  toastr.info('Updated draft', `Draft "${flat.headline}" has been updated.`);
  
}//updateDraftFlatInLocalStorage

//================================
//================================

export const updateDraftFlatOnFirebase = flat => {
  console.log("Update flat on FireBase")
}

//================================
//================================

export const getDraftFlatFromLocalStorage = draftId => {

  let flatDraftArray = getFlatArrayFromLocalStorage();

  let foundDraft =  flatDraftArray.filter( item => item.draftId === draftId )[0];

  return foundDraft;

}
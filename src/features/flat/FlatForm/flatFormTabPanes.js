//This file defines the tab panes for the FlatForm

import React from 'react';
import { Tab } from 'semantic-ui-react';

import Location from './flatFormPages/Location';
import Details from './flatFormPages/Details';
import Payment from './flatFormPages/Payment';
import Features from './flatFormPages/Features';
import Description from './flatFormPages/Description';
import Pictures from './flatFormPages/Pictures';

export default [
  {  
    menuItem: { key: 'edit', icon: 'edit', content: '' },
    render: () => (
    <Tab.Pane attached={true} style={{border: 0}} >
      <Description/>
    </Tab.Pane>
    ) 
  },
  {  
    menuItem: { key: 'map', icon: 'map', content: '' },
    render: () => (
      <Tab.Pane attached={true} style={{border: 0}} >
        <Location/>
      </Tab.Pane>
      ) 
  },
  {  
    menuItem: { key: 'home', icon: 'home', content: '' },
    render: () => (
      <Tab.Pane attached={true} style={{border: 0}} >
        <Details/>
      </Tab.Pane>
      ) 
  },    
  {  
    menuItem: { key: 'payment', icon: 'euro', content: '' },
    render: () => (
      <Tab.Pane attached={true} style={{border: 0}} >
        <Payment/>
      </Tab.Pane>
      ) 
  },
  {  
    menuItem: { key: 'list', icon: 'list', content: '' },
    render: () => (
      <Tab.Pane attached={true} style={{border: 0}} >
        <Features/>
      </Tab.Pane>
      ) 
  },
  {  
    menuItem: { key: 'picture', icon: 'picture', content: '' },
    render: () => (
      <Tab.Pane attached={true} style={{border: 0}} >
        <Pictures/>
      </Tab.Pane>
      ) 
  },     
]
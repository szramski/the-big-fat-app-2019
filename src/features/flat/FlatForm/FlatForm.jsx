import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {initialize, reduxForm} from 'redux-form';
import moment from 'moment';

//==== Design =====================
import { Tab } from 'semantic-ui-react';
import { Container, Row, Col, Card, CardHeader, CardBody, CardFooter } from 'reactstrap';
import './FlatForm.scss';

//==== Panes for the FlatForm =====================
import flatFormTabPanes from './flatFormTabPanes';

import ButtonRow from './ButtonRow';

import {
  createDraftFlatAndStoreInLocalStorage,
  getDraftFlatFromLocalStorage,
  updateDraftFlatInLocalStorage,
  updateDraftFlatOnFirebase
} from './flatFormFunctions';

import {
  createFlat
} from '../flatReducer/flatActions';

import {initialFlat} from '../../../app/util/formDataLists';

const mapState = state => {
  
  return {
    initialValues: initialFlat
  }
}

const actions = {
  createFlat
};

const FlatForm = ({initialValues, createFlat, match, dispatch}) => {

  useEffect( () => {

    //populate the flatForm with initial data, either pre-defined data
    //or data coming from an existing flat

    let flatId = match.params.id;
 
    if(flatId) {
      /* We're editing an existing flat! */

      let flatToEdit = {}; //get flat data from DB
      dispatch(initialize('flatForm', flatToEdit));

    } else {
      /* We're creating a new flat! */
      //First, we defining the date for create flat:
      initialValues.availableFrom = moment();

      //Next, we initialize the form with the initial Flat:
      dispatch(initialize('flatForm', initialValues));
    }//if

  }, []);

//==========================

  const submitFlat = flat => {

    if (!flat.id) {
      /* create new flat */
      createFlat(flat);
    } else {
      /* update existing flat */
      
    }

    //and also we want to set the pristine state of the form data:
    dispatch(initialize('flatForm', flat));

  }//submitFlat

//==========================
  return (
    <Container className="FlatForm" fluid>
      <Row className="FlatFormRow">
        <Col xs={11} sm={11} md={12} xl={8} className="mx-auto p-0">
          <Card className="my-4 szr-card">
            <CardHeader
              className="p-2 shadow-sm szr-flat-item-header text-truncate text-center h5">
              <span className="m-0">Announcement Details</span>
            </CardHeader>
            <CardBody className="p-0 pb-3">
              <Tab 
                menu={{ pointing: true }} 
                panes={flatFormTabPanes} 
              />
            </CardBody>
            <CardFooter>
              <ButtonRow submitFlat={submitFlat}/>
            </CardFooter>
          </Card>           
        </Col>
      </Row>
    </Container>
  )
}

export default reduxForm({
  form: 'flatForm',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
})(
  connect(mapState, actions)(FlatForm)
);
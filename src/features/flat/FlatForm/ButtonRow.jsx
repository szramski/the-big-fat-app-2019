import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { reduxForm } from 'redux-form';

//==== Design ====================================================
import { Container, Row, Col } from 'reactstrap';
import { Form, Button } from 'semantic-ui-react';

import validate from './validate';

const mapState = state => ({
  syncErrors: state.form.flatForm.syncErrors,
  writingFlat: state.async.loading,

})

const ButtonRow = ({
  history, 
  pristine, 
  submitting, 
  handleSubmit, 
  submitFlat, 
  syncErrors,
  writingFlat
}) => {

/*   const convertSyncErrorsToArray = syncErrors => {

    let arr = [];

    for (let property in syncErrors) {
      arr.push(syncErrors[property])
    }//for

    return arr;
  } */

  return(
    <Form onSubmit={handleSubmit(submitFlat)}>
  <Container fluid>
    <Row className="">
      <Col xs={6} sm={6} md={6} lg={6} className="p-0">
      <Button
          className="ui red button"
          onClick={() => history.push('/myans')}
          disabled={writingFlat}
        >
          <i className="icon check"></i>
          Close
        </Button>
      </Col>
      <Col xs={6} sm={6} md={6} lg={6} className="p-0">
      <Button
          type="submit"
          disabled={!!syncErrors || pristine || submitting }
          loading={writingFlat}
          className="ui teal button float-right">
          <i className="icon save"></i>
          Save
        </Button>
      </Col>
    </Row>
{/*     {
    syncErrors && 
      <Row className="mt-3">
        <Col xs={12} sm={12} md={12} lg={12} 
          className="p-1"
          style={{
            backgroundColor: '#ECEFF1',
            border: '1px solid #f44336'
            }}
          >
          <strong>Please note</strong>
        <ul>
          {
            convertSyncErrorsToArray(syncErrors).map( (error, index) => 
                                                      <li key={index}>{error}</li>
                                                    )
          }
        </ul>
        </Col>
      </Row>
  }    */} 
  </Container>
  </Form>
  )
}

export default reduxForm({
  form: 'flatForm',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate  
})(
  withRouter(
  connect(mapState)(ButtonRow)
  )
);
const validate = values => {
  const errors = {};

  if (!values.headline) {
    errors.headline = 'The headline is required.'
  } 

 if (values.address_details && (values.address_details.latlng.lat === 0 )){
     errors.addressSearchString = 'The address is required.'
}

if(!values.yearConstruction) {
  errors.yearConstruction = 'The year of construction is required and must be either a number or a hyphen: "-"';
}

if(!values.yearRenovation) {
  errors.yearConstruction = 'The year of of last renovation is required and must be either a number or a hyphen: "-"';
}

if(!values.surface) {
  errors.surface = 'Required';
}

if(!values.surfaceNet) {
  errors.surfaceNet = 'Required';
}

if(!values.floor) {
  errors.floor = 'Required';
}

if(!values.floorTotal) {
  errors.floorTotal = 'Required';
}


//================================
//================================


if(values.yearConstruction !== '-') {
  if(isNaN(values.yearConstruction)){
    errors.yearConstruction = 'The year of construction must be either a number or a hyphen: "-"';
  }
}

if(values.yearRenovation !== '-') {
  if(isNaN(values.yearRenovation)){
    errors.yearRenovation = 'The year of last renovation must be either a number or a hyphen: "-"';
  } else if (!isNaN(values.yearConstruction) && (values.yearRenovation < values.yearConstruction)){
    errors.yearRenovation = 'The last renovation should take place after the construction.';
  }
}

if(values.surface <= 0 ) {
  errors.surface = 'Please enter positive numbers larger than 0 for the surface';
}

if(values.surfaceNet <= 0 ) {
  errors.surface = 'Please enter positive numbers larger than 0 for the net surface';
}

if(values.surface < values.surfaceNet) {
  errors.surfaceNet = 'Must be smaller than the total surface.';
}

if(values.floorTotal < values.floor) {
  errors.floor = 'Must be smaller than the total number of floors.';
}





  /* if (!values.age) {
    errors.age = 'Required'
  } else if (isNaN(Number(values.age))) {
    errors.age = 'Must be a number'
  } else if (Number(values.age) < 18) {
    errors.age = 'Sorry, you must be at least 18 years old'
  } */

  return errors
}

export default validate;
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import ReduxToastr from 'react-redux-toastr'

//=====================================
import moment from 'moment';
import 'moment/locale/en-gb';
import 'moment/locale/en-ie';
import 'moment/locale/de';
import 'moment/locale/es';
import 'moment/locale/pl';


import 'react-dates/initialize';
import {reset} from 'redux-form';

//=====================================
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'semantic-ui-css/semantic.min.css';
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'
import './index.css';

//=====================================
import App from './app/layout/App';
import ScrollToTop from './app/util/ScrollToTop/ScrollToTop';

//=====================================
import { Provider } from 'react-redux';
import { configureStore } from './app/redux/configureStore';
const store = configureStore();

//When the app first loads, we reset any previous values
//in the form reducer;
store.dispatch(reset('flatForm'));

//moment.locale('de');
//moment.locale('en');
//moment.locale('en-gb');
//moment.locale('es');
moment.locale('de');

const jsx = (
  <Provider store={store}>
    <BrowserRouter>
      <ScrollToTop>
        <ReduxToastr
          timeOut={4000}
          newestOnTop={true}
          position="top-left"
          transitionIn="fadeIn"
          transitionOut="fadeOut"
          progressBar
          closeOnToastrClick
        />
        <App /> 
      </ScrollToTop>
    </BrowserRouter>  
  </Provider>

);

ReactDOM.render(jsx, document.getElementById('root'));


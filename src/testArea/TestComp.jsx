import React, {useEffect} from 'react';
import { connect } from 'react-redux';

//======== ReduxForm and data validation =========
import { reduxForm, Field, initialize } from 'redux-form';
import { combineValidators, isRequired } from 'revalidate';

//My own ReduxForm Field component
import TextInput from '../app/util/formFields/TextInput';

//======== Layout stuff =========
import { Container, Row, Col } from 'reactstrap';
import { Form } from 'semantic-ui-react';

//======== Initialize form data =========
//for example with data from DB
const mapStateToProps = state => {

  //a simple data object
  let initData = { headline: 'Star Trek rocks!' };

  return {
    initialValues: initData
  }
}//mapStateToProps

//======== define the validation function =========
const validate = combineValidators({
  headline: isRequired({message: 'The headline is required'})
});

//======== onSubmit function (for ex: write data to DB) =========
const onSubmit = values => {
  console.log('Submitting:', values)
}

// ================================================================
// =========== our beautiful component: ===========================

const TestComp = ({
  initialValues, //comes from mapStateToProps
  handleSubmit, //from reduxForm
  submitting, //from reduxForm, indicates that the form is being submitted
  invalid, //from reduxForm, indicates that the validation criteria is not met
  pristine, //from reduxForm, indicates that the initial form data was not changed
  dispatch, //from redux, dispatches an action
}) => {

  useEffect( () => {
    //componentDidMount (executes only on first load!)
    //We send the initialValues to the form
    dispatch(initialize('mrDataForm', initialValues));
  }, [])

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>            
    <Container className="p-5">  
      <Row>
        <Col className="p-0 pt-5">
        {/* a reusable text field component for reduxForm */}
          <Field 
            name="headline"
            label="Headline"
            type="text"
            component={TextInput}
            placeholder="Enter a headline for the announcement."
          />  
          <button 
            type="submit"
            className="btn btn-sm btn-success"
            disabled={invalid || submitting || pristine}
            //The button will be disabed if the form data is invalid
            //or if it's currently being submitted or if it hasn't
            //been changed so far.
          >
            Make it so!
          </button>
        </Col>
      </Row>
      </Container>
      </Form>
  )//return
}//TestComp

//Now we export a HOC so that we can connect our component to redux
//and so that we can use reduxForm here
export default connect(mapStateToProps)(
  reduxForm({
    form: 'mrDataForm',
    validate
  })(TestComp)
)

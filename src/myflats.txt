import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';

import { Tab, Button } from 'semantic-ui-react';
import { 
  Container, Row, Col, Table, Card, CardHeader, CardFooter, CardBody 
} from 'reactstrap';


import './MyFlats.scss';

import {
  getFlatArrayFromLocalStorage,
  setFlatArrayInLocalStorage,
} from '../../../app/util/utilFunctions';

import {writeFlatToFirestore} from '../../flat/flatReducer/flatActions';

const actions = { writeFlatToFirestore };




const MyFlats = ({writeFlatToFirestore}) => {

  const [flatArray, setFlatArray] = useState([]);
  const [activeTabNumber, setActiveTabNumber] = useState(0); //for texts on tab pane

  useEffect(() => {
    //gather flats from localStrage and set them into component state
    setFlatArray(getFlatArrayFromLocalStorage());
    setActiveTabNumber(0); //redundant but ok.
  }, []);

  const panes = [
    { menuItem: 'Drafts', pane: { key: 'drafts' } },
    { menuItem: 'Published & hidden', pane: { key: 'hiddenAnnouncements' } },
    { menuItem: 'Published & active', pane: { key: 'activeAnnouncements' } },
  ]

  const changeTab = (e, data) => {
    //this.props.getUserEvents(this.props.userUid, data.activeIndex);

    if (data.activeIndex === 0) {
      setFlatArray(getFlatArrayFromLocalStorage());
      setActiveTabNumber(0);
    }

    if (data.activeIndex === 1) {
      //get published, hidden flats from DB
      setFlatArray([]);
      setActiveTabNumber(1);
    }

    if (data.activeIndex === 2) {
      //get published, active flats from DB
      setFlatArray([]);
      setActiveTabNumber(2);
    }

  }

  const handleDraftDelete = id => () => {

    //add all items from flatArray where the draftId doesn't match the provided id
    let newArr = flatArray.filter(item => item.draftId !== id);

    //set that new array in state to trigger a refresh of the flat list
    setFlatArray(newArr);

    //set that new array in localStorage 
    setFlatArrayInLocalStorage(newArr);

  }//handleDraftDelete

  const handlePublishFlatToFirebase = draftId => async () => {

    //find the draft that we want to publish
    const flatToPublish = getFlatArrayFromLocalStorage().filter( item => item.draftId === draftId)[0];

    //pass the flat object to the corresponding action and get true or false based on the result:
    const writeToFirestoreOK = await writeFlatToFirestore(flatToPublish);

    //If the write to the DB was ok, then delete the draft from the draftArray
    if(writeToFirestoreOK){
      handleDraftDelete(draftId)();
    }

  }//handlePublishFlatToFirebase

  const handleFlatOnFirebaseDelete = id => () => {

  }


  return (
    <Container className="MyFlats" fluid>
      <Row className="MyFlatsRow">
        <Col xs={11} sm={11} md={8} xl={8} className="mx-auto p-0">

          <Card className="my-4 szr-card">
            <CardHeader
              className="p-2 shadow-sm szr-flat-item-header text-truncate text-center h5">
              <span className="m-0">My Announcements</span>
            </CardHeader>
            <CardBody className="p-0 pb-3">

              <Tab
                menu={{ secondary: true, pointing: true }}
                panes={panes}
                onTabChange={(e, data) => changeTab(e, data)}
              />

              {
                activeTabNumber === 0 &&
                <p className="my-4 text-center">
                  These announcement drafts exist only in the cache of your browser.
                  Clearing the browser cache will delte all of them.
                  </p>
              }
              {
                activeTabNumber === 1 &&
                <p className="my-4 text-center">
                  These announcements have been published but are not visible to anyone.<br />
                  We charge you <strong>1ct per day</strong> for each one of the announcements listed here.
                  </p>
              }
              {
                activeTabNumber === 2 &&
                <p className="my-4 text-center">
                  These announcements have been published and are visible to everyone.<br />
                  We charge you <strong>15cts per day</strong> for each one of the announcements listed here.
                  </p>
              }
              {
                flatArray.length === 0 ?
                  <p className="my-3 text-center">No announcements found.</p>
                  :
                  <Table responsive hover className="bg-white">
                    <thead>
                      <tr>
                        <th>Ref #</th>
                        <th>Headline</th>
                        <th>Street</th>
                        <th>City</th>
                        <th colSpan="3" className="text-center">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        flatArray.map(({
                          id,
                          draftId,
                          headline,
                          referenceId,
                          address_details: { streetName, streetNumber, city }
                        }) => (
                            <tr key={draftId}>
                              <td>{referenceId === '' ? '-' : `${referenceId}`}</td>
                              <td className="text-truncate">{headline}</td>
                              <td>{streetName === 'n/a' ? '-' : `${streetName} ${streetNumber}`}</td>
                              <td>{city}</td>
                              <td>
                                <Button
                                  as={Link}
                                  to={`/flatform/draft/${draftId}`}
                                  content='Edit'
                                  color='yellow'
                                  size='mini'
                                />
                              </td>
                              <td>
                                <Button
                                  onClick={handlePublishFlatToFirebase(draftId)}
                                  content='Publish (active)'
                                  color='green'
                                  size='mini'
                                />
                              </td>
                              <td>
                                {!id && draftId &&
                                  <Button
                                    onClick={handleDraftDelete(draftId)}
                                    content='Delete Draft'
                                    color='red'
                                    size='mini'
                                  />
                                }
                                {id && !draftId &&
                                  <Button
                                    onClick={handleFlatOnFirebaseDelete(id)}
                                    content='Delete'
                                    color='red'
                                    size='mini'
                                  />
                                }
                              </td>
                            </tr>)
                        )//map
                      }
                    </tbody>
                  </Table>
              }

            </CardBody>
            <CardFooter>
              <Button as={Link} to='/flatform' content='New Announcement' color='teal' floated='right' />
            </CardFooter>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

export default connect(null, actions)(MyFlats);